<table border="100">
    <thead>
    <tr></tr>
    <tr>
        <th colspan=49><strong>Data Kehadiran Karyawan Tahun : {{$year}}</strong></th>
    </tr>
    <tr>
        <th colspan=49><strong>{{$pernr}} - {{$surname}} - ({{$initial}})</strong></th>
    </tr>
    <tr></tr>
    <tr>
        <th rowspan="3">
            Tanggal
        </th>
        <th colspan="4">
            Jan - Feb {{$year}}
        </th>
        <th colspan="4">
            Feb - Mar {{$year}}
        </th>
        <th colspan="4">
            Mar - Apr {{$year}}
        </th>
        <th colspan="4">
            Apr - Mei {{$year}}
        </th>
        <th colspan="4">
            Mei- Jun {{$year}}
        </th>
        <th colspan="4">
            Jun - Jul {{$year}}
        </th>
        <th rowspan="3">
            Tanggal
        </th>
        <th colspan="4">
            Jul - Agu {{$year}}
        </th>
        <th colspan="4">
            Agu - Sep {{$year}}
        </th>
        <th colspan="4">
            Sep - Okt {{$year}}
        </th>
        <th colspan="4">
            Okt - Nov {{$year}}
        </th>
        <th colspan="4">
            Nov - Des {{$year}}
        </th>
        <th colspan="4">
            Des - Jan {{$year+1}}
        </th>
    </tr>
    <tr>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
        <th colspan="2">Dtg.</th>
        <th colspan="2">Plg.</th>
    </tr>
    <tr>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
        <th>Wkt.</th>
        <th>Lok.</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $absen)
        <tr>
            @if($absen->days=='Kosong')
            <td></td>
            @elseif (in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
            <td style="text-align:left">{{ $absen->days }}</td>
            @else
            <td style="text-align:right">{{ $absen->days }}</td>
            @endif
            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->jan_feb_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                    <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jan_feb_in }}</td>
                @elseif((int) date('i', strtotime($absen->jan_feb_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                    <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jan_feb_in }}</td>
                @else
                    <td colspan="4" style="text-align:right">{{ $absen->jan_feb_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '1':'2'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->jan_feb_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '1':'2'),$absen->days,$year) && $absen->jan_feb_in === '-' && $absen->jan_feb_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '1':'2').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->jan_feb_in !== 'Holiday' && $absen->jan_feb_in==$absen->jan_feb_out && ( $absen->jan_feb_in == 'Tugas Kantor' || $absen->jan_feb_in == 'PD Luar Negeri' || $absen->jan_feb_in == 'Seminar / Konferensi' || $absen->jan_feb_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->jan_feb_in }}</td>
                @elseif($absen->jan_feb_out !== '-' && $absen->jan_feb_out !== 'Holiday' && $absen->jan_feb_out !== 'Tugas Kantor' && $absen->jan_feb_out !== 'PD Luar Negeri' && $absen->jan_feb_out !== 'Seminar / Konferensi' && $absen->jan_feb_out !== 'Pelatihan / Workshop' && $absen->jan_feb_out !== 'Cuti' && $absen->jan_feb_out !== 'Sakit' && $absen->jan_feb_out !== 'Izin' && $absen->jan_feb_in==$absen->jan_feb_out && ((int) date('H', strtotime($absen->jan_feb_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @elseif($absen->jan_feb_out !== '-' && $absen->jan_feb_out !== 'Holiday' && $absen->jan_feb_out !== 'Tugas Kantor' && $absen->jan_feb_out !== 'PD Luar Negeri' && $absen->jan_feb_out !== 'Seminar / Konferensi' && $absen->jan_feb_out !== 'Pelatihan / Workshop' && $absen->jan_feb_out !== 'Cuti' && $absen->jan_feb_out !== 'Sakit' && $absen->jan_feb_out !== 'Izin' && $absen->jan_feb_in==$absen->jan_feb_out && ((int) date('H', strtotime($absen->jan_feb_in)) < 13))
                <td>{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->jan_feb_in==$absen->jan_feb_out && $absen->jan_feb_in != '-' && ($absen->jan_feb_in == 'Izin' || $absen->jan_feb_in == 'Cuti' || $absen->jan_feb_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->jan_feb_in }}</td>
                @elseif($absen->jan_feb_in==$absen->jan_feb_out && ($absen->jan_feb_in == '-' || $absen->jan_feb_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->jan_feb_in == 'Izin' || $absen->jan_feb_in == 'Cuti' || $absen->jan_feb_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @elseif($absen->jan_feb_out == 'Izin' || $absen->jan_feb_out == 'Cuti' || $absen->jan_feb_out == 'Sakit')
                <td>{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->jan_feb_out }}</td>
                @elseif(((int) date('H', strtotime($absen->jan_feb_in)) >= 8) && ((int) date('H', strtotime($absen->jan_feb_out)) < 13)
                && $absen->jan_feb_in !== 'Izin' && $absen->jan_feb_in !== 'Cuti' && $absen->jan_feb_in !== 'Sakit' && $absen->jan_feb_in !== 'UPL' && $absen->jan_feb_in !== 'Holiday' && $absen->jan_feb_in !== 'Tugas Kantor' && $absen->jan_feb_in !== 'PD Luar Negeri' && $absen->jan_feb_in !== 'Seminar / Konferensi' && $absen->jan_feb_in !== 'Pelatihan / Workshop'
                && $absen->jan_feb_out !== 'Izin' && $absen->jan_feb_out !== 'Cuti' && $absen->jan_feb_out !== 'Sakit' && $absen->jan_feb_out !== 'UPL' && $absen->jan_feb_out !== 'Holiday' && $absen->jan_feb_out !== 'Tugas Kantor' && $absen->jan_feb_out !== 'PD Luar Negeri' && $absen->jan_feb_out !== 'Seminar / Konferensi' && $absen->jan_feb_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->jan_feb_in)) >= 8) && ((int) date('H', strtotime($absen->jan_feb_out)) < 13)
                && $absen->jan_feb_in !== 'Izin' && $absen->jan_feb_in !== 'Cuti' && $absen->jan_feb_in !== 'Sakit' && $absen->jan_feb_in !== 'UPL' && $absen->jan_feb_in !== 'Holiday' && $absen->jan_feb_in !== 'Tugas Kantor' && $absen->jan_feb_in !== 'PD Luar Negeri' && $absen->jan_feb_in !== 'Seminar / Konferensi' && $absen->jan_feb_in !== 'Pelatihan / Workshop'
                && $absen->jan_feb_out !== 'Izin' && $absen->jan_feb_out !== 'Cuti' && $absen->jan_feb_out !== 'Sakit' && $absen->jan_feb_out !== 'UPL' && $absen->jan_feb_out !== 'Holiday' && $absen->jan_feb_out !== 'Tugas Kantor' && $absen->jan_feb_out !== 'PD Luar Negeri' && $absen->jan_feb_out !== 'Seminar / Konferensi' && $absen->jan_feb_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->jan_feb_in)) >= 8) && !((int) date('H', strtotime($absen->jan_feb_out)) < 13)
                && $absen->jan_feb_in !== 'Izin' && $absen->jan_feb_in !== 'Cuti' && $absen->jan_feb_in !== 'Sakit' && $absen->jan_feb_in !== 'UPL' && $absen->jan_feb_in !== 'Holiday' && $absen->jan_feb_in !== 'Tugas Kantor' && $absen->jan_feb_in !== 'PD Luar Negeri' && $absen->jan_feb_in !== 'Seminar / Konferensi' && $absen->jan_feb_in !== 'Pelatihan / Workshop'
                && $absen->jan_feb_out !== 'Izin' && $absen->jan_feb_out !== 'Cuti' && $absen->jan_feb_out !== 'Sakit' && $absen->jan_feb_out !== 'UPL' && $absen->jan_feb_out !== 'Holiday' && $absen->jan_feb_out !== 'Tugas Kantor' && $absen->jan_feb_out !== 'PD Luar Negeri' && $absen->jan_feb_out !== 'Seminar / Konferensi' && $absen->jan_feb_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td>{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @else
                <td>{{ $absen->jan_feb_in }}</td>
                <td>{{ $absen->jan_feb_in_loc }}</td>
                <td>{{ $absen->jan_feb_out }}</td>
                <td>{{ $absen->jan_feb_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->feb_mar_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->feb_mar_in }}</td>
                @elseif((int) date('i', strtotime($absen->feb_mar_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->feb_mar_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->feb_mar_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '2':'3'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->feb_mar_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '2':'3'),$absen->days,$year) && $absen->feb_mar_in === '-' && $absen->feb_mar_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '2':'3').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->feb_mar_in !== 'Holiday' && $absen->feb_mar_in==$absen->feb_mar_out && ( $absen->feb_mar_in == 'Tugas Kantor' || $absen->feb_mar_in == 'PD Luar Negeri' || $absen->feb_mar_in == 'Seminar / Konferensi' || $absen->feb_mar_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->feb_mar_in }}</td>
                @elseif($absen->feb_mar_out !== '-' && $absen->feb_mar_out !== 'Holiday' && $absen->feb_mar_out !== 'Tugas Kantor' && $absen->feb_mar_out !== 'PD Luar Negeri' && $absen->feb_mar_out !== 'Seminar / Konferensi' && $absen->feb_mar_out !== 'Pelatihan / Workshop' && $absen->feb_mar_out !== 'Cuti' && $absen->feb_mar_out !== 'Sakit' && $absen->feb_mar_out !== 'Izin' && $absen->feb_mar_in==$absen->feb_mar_out && ((int) date('H', strtotime($absen->feb_mar_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @elseif($absen->feb_mar_out !== '-' && $absen->feb_mar_out !== 'Holiday' && $absen->feb_mar_out !== 'Tugas Kantor' && $absen->feb_mar_out !== 'PD Luar Negeri' && $absen->feb_mar_out !== 'Seminar / Konferensi' && $absen->feb_mar_out !== 'Pelatihan / Workshop' && $absen->feb_mar_out !== 'Cuti' && $absen->feb_mar_out !== 'Sakit' && $absen->feb_mar_out !== 'Izin' && $absen->feb_mar_in==$absen->feb_mar_out && ((int) date('H', strtotime($absen->feb_mar_in)) < 13))
                <td>{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->feb_mar_in==$absen->feb_mar_out && $absen->feb_mar_in != '-' && ($absen->feb_mar_in == 'Izin' || $absen->feb_mar_in == 'Cuti' || $absen->feb_mar_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->feb_mar_in }}</td>
                @elseif($absen->feb_mar_in==$absen->feb_mar_out && ($absen->feb_mar_in == '-' || $absen->feb_mar_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->feb_mar_in == 'Izin' || $absen->feb_mar_in == 'Cuti' || $absen->feb_mar_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @elseif($absen->feb_mar_out == 'Izin' || $absen->feb_mar_out == 'Cuti' || $absen->feb_mar_out == 'Sakit')
                <td>{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->feb_mar_out }}</td>
                @elseif(((int) date('H', strtotime($absen->feb_mar_in)) >= 8) && ((int) date('H', strtotime($absen->feb_mar_out)) < 13)
                && $absen->feb_mar_in !== 'Izin' && $absen->feb_mar_in !== 'Cuti' && $absen->feb_mar_in !== 'Sakit' && $absen->feb_mar_in !== 'UPL' && $absen->feb_mar_in !== 'Holiday' && $absen->feb_mar_in !== 'Tugas Kantor' && $absen->feb_mar_in !== 'PD Luar Negeri' && $absen->feb_mar_in !== 'Seminar / Konferensi' && $absen->feb_mar_in !== 'Pelatihan / Workshop'
                && $absen->feb_mar_out !== 'Izin' && $absen->feb_mar_out !== 'Cuti' && $absen->feb_mar_out !== 'Sakit' && $absen->feb_mar_out !== 'UPL' && $absen->feb_mar_out !== 'Holiday' && $absen->feb_mar_out !== 'Tugas Kantor' && $absen->feb_mar_out !== 'PD Luar Negeri' && $absen->feb_mar_out !== 'Seminar / Konferensi' && $absen->feb_mar_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->feb_mar_in)) >= 8) && ((int) date('H', strtotime($absen->feb_mar_out)) < 13)
                && $absen->feb_mar_in !== 'Izin' && $absen->feb_mar_in !== 'Cuti' && $absen->feb_mar_in !== 'Sakit' && $absen->feb_mar_in !== 'UPL' && $absen->feb_mar_in !== 'Holiday' && $absen->feb_mar_in !== 'Tugas Kantor' && $absen->feb_mar_in !== 'PD Luar Negeri' && $absen->feb_mar_in !== 'Seminar / Konferensi' && $absen->feb_mar_in !== 'Pelatihan / Workshop'
                && $absen->feb_mar_out !== 'Izin' && $absen->feb_mar_out !== 'Cuti' && $absen->feb_mar_out !== 'Sakit' && $absen->feb_mar_out !== 'UPL' && $absen->feb_mar_out !== 'Holiday' && $absen->feb_mar_out !== 'Tugas Kantor' && $absen->feb_mar_out !== 'PD Luar Negeri' && $absen->feb_mar_out !== 'Seminar / Konferensi' && $absen->feb_mar_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->feb_mar_in)) >= 8) && !((int) date('H', strtotime($absen->feb_mar_out)) < 13)
                && $absen->feb_mar_in !== 'Izin' && $absen->feb_mar_in !== 'Cuti' && $absen->feb_mar_in !== 'Sakit' && $absen->feb_mar_in !== 'UPL' && $absen->feb_mar_in !== 'Holiday' && $absen->feb_mar_in !== 'Tugas Kantor' && $absen->feb_mar_in !== 'PD Luar Negeri' && $absen->feb_mar_in !== 'Seminar / Konferensi' && $absen->feb_mar_in !== 'Pelatihan / Workshop'
                && $absen->feb_mar_out !== 'Izin' && $absen->feb_mar_out !== 'Cuti' && $absen->feb_mar_out !== 'Sakit' && $absen->feb_mar_out !== 'UPL' && $absen->feb_mar_out !== 'Holiday' && $absen->feb_mar_out !== 'Tugas Kantor' && $absen->feb_mar_out !== 'PD Luar Negeri' && $absen->feb_mar_out !== 'Seminar / Konferensi' && $absen->feb_mar_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td>{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @else
                <td>{{ $absen->feb_mar_in }}</td>
                <td>{{ $absen->feb_mar_in_loc }}</td>
                <td>{{ $absen->feb_mar_out }}</td>
                <td>{{ $absen->feb_mar_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->mar_apr_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->mar_apr_in }}</td>
                @elseif((int) date('i', strtotime($absen->mar_apr_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->mar_apr_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->mar_apr_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '3':'4'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->mar_apr_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '3':'4'),$absen->days,$year) && $absen->mar_apr_in === '-' && $absen->mar_apr_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '3':'4').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->mar_apr_in !== 'Holiday' && $absen->mar_apr_in==$absen->mar_apr_out && ( $absen->mar_apr_in == 'Tugas Kantor' || $absen->mar_apr_in == 'PD Luar Negeri' || $absen->mar_apr_in == 'Seminar / Konferensi' || $absen->mar_apr_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->mar_apr_in }}</td>
                @elseif($absen->mar_apr_out !== '-' && $absen->mar_apr_out !== 'Holiday' && $absen->mar_apr_out !== 'Tugas Kantor' && $absen->mar_apr_out !== 'PD Luar Negeri' && $absen->mar_apr_out !== 'Seminar / Konferensi' && $absen->mar_apr_out !== 'Pelatihan / Workshop' && $absen->mar_apr_out !== 'Cuti' && $absen->mar_apr_out !== 'Sakit' && $absen->mar_apr_out !== 'Izin' && $absen->mar_apr_in==$absen->mar_apr_out && ((int) date('H', strtotime($absen->mar_apr_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @elseif($absen->mar_apr_out !== '-' && $absen->mar_apr_out !== 'Holiday' && $absen->mar_apr_out !== 'Tugas Kantor' && $absen->mar_apr_out !== 'PD Luar Negeri' && $absen->mar_apr_out !== 'Seminar / Konferensi' && $absen->mar_apr_out !== 'Pelatihan / Workshop' && $absen->mar_apr_out !== 'Cuti' && $absen->mar_apr_out !== 'Sakit' && $absen->mar_apr_out !== 'Izin' && $absen->mar_apr_in==$absen->mar_apr_out && ((int) date('H', strtotime($absen->mar_apr_in)) < 13))
                <td>{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->mar_apr_in==$absen->mar_apr_out && $absen->mar_apr_in != '-' && ($absen->mar_apr_in == 'Izin' || $absen->mar_apr_in == 'Cuti' || $absen->mar_apr_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->mar_apr_in }}</td>
                @elseif($absen->mar_apr_in==$absen->mar_apr_out && ($absen->mar_apr_in == '-' || $absen->mar_apr_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->mar_apr_in == 'Izin' || $absen->mar_apr_in == 'Cuti' || $absen->mar_apr_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @elseif($absen->mar_apr_out == 'Izin' || $absen->mar_apr_out == 'Cuti' || $absen->mar_apr_out == 'Sakit')
                <td>{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->mar_apr_out }}</td>
                @elseif(((int) date('H', strtotime($absen->mar_apr_in)) >= 8) && ((int) date('H', strtotime($absen->mar_apr_out)) < 13)
                && $absen->mar_apr_in !== 'Izin' && $absen->mar_apr_in !== 'Cuti' && $absen->mar_apr_in !== 'Sakit' && $absen->mar_apr_in !== 'UPL' && $absen->mar_apr_in !== 'Holiday' && $absen->mar_apr_in !== 'Tugas Kantor' && $absen->mar_apr_in !== 'PD Luar Negeri' && $absen->mar_apr_in !== 'Seminar / Konferensi' && $absen->mar_apr_in !== 'Pelatihan / Workshop'
                && $absen->mar_apr_out !== 'Izin' && $absen->mar_apr_out !== 'Cuti' && $absen->mar_apr_out !== 'Sakit' && $absen->mar_apr_out !== 'UPL' && $absen->mar_apr_out !== 'Holiday' && $absen->mar_apr_out !== 'Tugas Kantor' && $absen->mar_apr_out !== 'PD Luar Negeri' && $absen->mar_apr_out !== 'Seminar / Konferensi' && $absen->mar_apr_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->mar_apr_in)) >= 8) && ((int) date('H', strtotime($absen->mar_apr_out)) < 13)
                && $absen->mar_apr_in !== 'Izin' && $absen->mar_apr_in !== 'Cuti' && $absen->mar_apr_in !== 'Sakit' && $absen->mar_apr_in !== 'UPL' && $absen->mar_apr_in !== 'Holiday' && $absen->mar_apr_in !== 'Tugas Kantor' && $absen->mar_apr_in !== 'PD Luar Negeri' && $absen->mar_apr_in !== 'Seminar / Konferensi' && $absen->mar_apr_in !== 'Pelatihan / Workshop'
                && $absen->mar_apr_out !== 'Izin' && $absen->mar_apr_out !== 'Cuti' && $absen->mar_apr_out !== 'Sakit' && $absen->mar_apr_out !== 'UPL' && $absen->mar_apr_out !== 'Holiday' && $absen->mar_apr_out !== 'Tugas Kantor' && $absen->mar_apr_out !== 'PD Luar Negeri' && $absen->mar_apr_out !== 'Seminar / Konferensi' && $absen->mar_apr_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->mar_apr_in)) >= 8) && !((int) date('H', strtotime($absen->mar_apr_out)) < 13)
                && $absen->mar_apr_in !== 'Izin' && $absen->mar_apr_in !== 'Cuti' && $absen->mar_apr_in !== 'Sakit' && $absen->mar_apr_in !== 'UPL' && $absen->mar_apr_in !== 'Holiday' && $absen->mar_apr_in !== 'Tugas Kantor' && $absen->mar_apr_in !== 'PD Luar Negeri' && $absen->mar_apr_in !== 'Seminar / Konferensi' && $absen->mar_apr_in !== 'Pelatihan / Workshop'
                && $absen->mar_apr_out !== 'Izin' && $absen->mar_apr_out !== 'Cuti' && $absen->mar_apr_out !== 'Sakit' && $absen->mar_apr_out !== 'UPL' && $absen->mar_apr_out !== 'Holiday' && $absen->mar_apr_out !== 'Tugas Kantor' && $absen->mar_apr_out !== 'PD Luar Negeri' && $absen->mar_apr_out !== 'Seminar / Konferensi' && $absen->mar_apr_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td>{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @else
                <td>{{ $absen->mar_apr_in }}</td>
                <td>{{ $absen->mar_apr_in_loc }}</td>
                <td>{{ $absen->mar_apr_out }}</td>
                <td>{{ $absen->mar_apr_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->apr_mei_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->apr_mei_in }}</td>
                @elseif((int) date('i', strtotime($absen->apr_mei_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->apr_mei_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->apr_mei_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '4':'5'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->apr_mei_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '4':'5'),$absen->days,$year) && $absen->apr_mei_in === '-' && $absen->apr_mei_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '4':'5').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->apr_mei_in !== 'Holiday' && $absen->apr_mei_in==$absen->apr_mei_out && ( $absen->apr_mei_in == 'Tugas Kantor' || $absen->apr_mei_in == 'PD Luar Negeri' || $absen->apr_mei_in == 'Seminar / Konferensi' || $absen->apr_mei_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->apr_mei_in }}</td>
                @elseif($absen->apr_mei_out !== '-' && $absen->apr_mei_out !== 'Holiday' && $absen->apr_mei_out !== 'Tugas Kantor' && $absen->apr_mei_out !== 'PD Luar Negeri' && $absen->apr_mei_out !== 'Seminar / Konferensi' && $absen->apr_mei_out !== 'Pelatihan / Workshop' && $absen->apr_mei_out !== 'Cuti' && $absen->apr_mei_out !== 'Sakit' && $absen->apr_mei_out !== 'Izin' && $absen->apr_mei_in==$absen->apr_mei_out && ((int) date('H', strtotime($absen->apr_mei_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @elseif($absen->apr_mei_out !== '-' && $absen->apr_mei_out !== 'Holiday' && $absen->apr_mei_out !== 'Tugas Kantor' && $absen->apr_mei_out !== 'PD Luar Negeri' && $absen->apr_mei_out !== 'Seminar / Konferensi' && $absen->apr_mei_out !== 'Pelatihan / Workshop' && $absen->apr_mei_out !== 'Cuti' && $absen->apr_mei_out !== 'Sakit' && $absen->apr_mei_out !== 'Izin' && $absen->apr_mei_in==$absen->apr_mei_out && ((int) date('H', strtotime($absen->apr_mei_in)) < 13))
                <td>{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->apr_mei_in==$absen->apr_mei_out && $absen->apr_mei_in != '-' && ($absen->apr_mei_in == 'Izin' || $absen->apr_mei_in == 'Cuti' || $absen->apr_mei_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->apr_mei_in }}</td>
                @elseif($absen->apr_mei_in==$absen->apr_mei_out && ($absen->apr_mei_in == '-' || $absen->apr_mei_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->apr_mei_in == 'Izin' || $absen->apr_mei_in == 'Cuti' || $absen->apr_mei_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @elseif($absen->apr_mei_out == 'Izin' || $absen->apr_mei_out == 'Cuti' || $absen->apr_mei_out == 'Sakit')
                <td>{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->apr_mei_out }}</td>
                @elseif(((int) date('H', strtotime($absen->apr_mei_in)) >= 8) && ((int) date('H', strtotime($absen->apr_mei_out)) < 13)
                && $absen->apr_mei_in !== 'Izin' && $absen->apr_mei_in !== 'Cuti' && $absen->apr_mei_in !== 'Sakit' && $absen->apr_mei_in !== 'UPL' && $absen->apr_mei_in !== 'Holiday' && $absen->apr_mei_in !== 'Tugas Kantor' && $absen->apr_mei_in !== 'PD Luar Negeri' && $absen->apr_mei_in !== 'Seminar / Konferensi' && $absen->apr_mei_in !== 'Pelatihan / Workshop'
                && $absen->apr_mei_out !== 'Izin' && $absen->apr_mei_out !== 'Cuti' && $absen->apr_mei_out !== 'Sakit' && $absen->apr_mei_out !== 'UPL' && $absen->apr_mei_out !== 'Holiday' && $absen->apr_mei_out !== 'Tugas Kantor' && $absen->apr_mei_out !== 'PD Luar Negeri' && $absen->apr_mei_out !== 'Seminar / Konferensi' && $absen->apr_mei_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->apr_mei_in)) >= 8) && ((int) date('H', strtotime($absen->apr_mei_out)) < 13)
                && $absen->apr_mei_in !== 'Izin' && $absen->apr_mei_in !== 'Cuti' && $absen->apr_mei_in !== 'Sakit' && $absen->apr_mei_in !== 'UPL' && $absen->apr_mei_in !== 'Holiday' && $absen->apr_mei_in !== 'Tugas Kantor' && $absen->apr_mei_in !== 'PD Luar Negeri' && $absen->apr_mei_in !== 'Seminar / Konferensi' && $absen->apr_mei_in !== 'Pelatihan / Workshop'
                && $absen->apr_mei_out !== 'Izin' && $absen->apr_mei_out !== 'Cuti' && $absen->apr_mei_out !== 'Sakit' && $absen->apr_mei_out !== 'UPL' && $absen->apr_mei_out !== 'Holiday' && $absen->apr_mei_out !== 'Tugas Kantor' && $absen->apr_mei_out !== 'PD Luar Negeri' && $absen->apr_mei_out !== 'Seminar / Konferensi' && $absen->apr_mei_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->apr_mei_in)) >= 8) && !((int) date('H', strtotime($absen->apr_mei_out)) < 13)
                && $absen->apr_mei_in !== 'Izin' && $absen->apr_mei_in !== 'Cuti' && $absen->apr_mei_in !== 'Sakit' && $absen->apr_mei_in !== 'UPL' && $absen->apr_mei_in !== 'Holiday' && $absen->apr_mei_in !== 'Tugas Kantor' && $absen->apr_mei_in !== 'PD Luar Negeri' && $absen->apr_mei_in !== 'Seminar / Konferensi' && $absen->apr_mei_in !== 'Pelatihan / Workshop'
                && $absen->apr_mei_out !== 'Izin' && $absen->apr_mei_out !== 'Cuti' && $absen->apr_mei_out !== 'Sakit' && $absen->apr_mei_out !== 'UPL' && $absen->apr_mei_out !== 'Holiday' && $absen->apr_mei_out !== 'Tugas Kantor' && $absen->apr_mei_out !== 'PD Luar Negeri' && $absen->apr_mei_out !== 'Seminar / Konferensi' && $absen->apr_mei_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td>{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @else
                <td>{{ $absen->apr_mei_in }}</td>
                <td>{{ $absen->apr_mei_in_loc }}</td>
                <td>{{ $absen->apr_mei_out }}</td>
                <td>{{ $absen->apr_mei_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->mei_jun_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->mei_jun_in }}</td>
                @elseif((int) date('i', strtotime($absen->mei_jun_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->mei_jun_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->mei_jun_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '5':'6'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->mei_jun_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '5':'6'),$absen->days,$year) && $absen->mei_jun_in === '-' && $absen->mei_jun_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '5':'6').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->mei_jun_in !== 'Holiday' && $absen->mei_jun_in==$absen->mei_jun_out && ( $absen->mei_jun_in == 'Tugas Kantor' || $absen->mei_jun_in == 'PD Luar Negeri' || $absen->mei_jun_in == 'Seminar / Konferensi' || $absen->mei_jun_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->mei_jun_in }}</td>
                @elseif($absen->mei_jun_out !== '-' && $absen->mei_jun_out !== 'Holiday' && $absen->mei_jun_out !== 'Tugas Kantor' && $absen->mei_jun_out !== 'PD Luar Negeri' && $absen->mei_jun_out !== 'Seminar / Konferensi' && $absen->mei_jun_out !== 'Pelatihan / Workshop' && $absen->mei_jun_out !== 'Cuti' && $absen->mei_jun_out !== 'Sakit' && $absen->mei_jun_out !== 'Izin' && $absen->mei_jun_in==$absen->mei_jun_out && ((int) date('H', strtotime($absen->mei_jun_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @elseif($absen->mei_jun_out !== '-' && $absen->mei_jun_out !== 'Holiday' && $absen->mei_jun_out !== 'Tugas Kantor' && $absen->mei_jun_out !== 'PD Luar Negeri' && $absen->mei_jun_out !== 'Seminar / Konferensi' && $absen->mei_jun_out !== 'Pelatihan / Workshop' && $absen->mei_jun_out !== 'Cuti' && $absen->mei_jun_out !== 'Sakit' && $absen->mei_jun_out !== 'Izin' && $absen->mei_jun_in==$absen->mei_jun_out && ((int) date('H', strtotime($absen->mei_jun_in)) < 13))
                <td>{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->mei_jun_in==$absen->mei_jun_out && $absen->mei_jun_in != '-' && ($absen->mei_jun_in == 'Izin' || $absen->mei_jun_in == 'Cuti' || $absen->mei_jun_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->mei_jun_in }}</td>
                @elseif($absen->mei_jun_in==$absen->mei_jun_out && ($absen->mei_jun_in == '-' || $absen->mei_jun_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->mei_jun_in == 'Izin' || $absen->mei_jun_in == 'Cuti' || $absen->mei_jun_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @elseif($absen->mei_jun_out == 'Izin' || $absen->mei_jun_out == 'Cuti' || $absen->mei_jun_out == 'Sakit')
                <td>{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->mei_jun_out }}</td>
                @elseif(((int) date('H', strtotime($absen->mei_jun_in)) >= 8) && ((int) date('H', strtotime($absen->mei_jun_out)) < 13)
                && $absen->mei_jun_in !== 'Izin' && $absen->mei_jun_in !== 'Cuti' && $absen->mei_jun_in !== 'Sakit' && $absen->mei_jun_in !== 'UPL' && $absen->mei_jun_in !== 'Holiday' && $absen->mei_jun_in !== 'Tugas Kantor' && $absen->mei_jun_in !== 'PD Luar Negeri' && $absen->mei_jun_in !== 'Seminar / Konferensi' && $absen->mei_jun_in !== 'Pelatihan / Workshop'
                && $absen->mei_jun_out !== 'Izin' && $absen->mei_jun_out !== 'Cuti' && $absen->mei_jun_out !== 'Sakit' && $absen->mei_jun_out !== 'UPL' && $absen->mei_jun_out !== 'Holiday' && $absen->mei_jun_out !== 'Tugas Kantor' && $absen->mei_jun_out !== 'PD Luar Negeri' && $absen->mei_jun_out !== 'Seminar / Konferensi' && $absen->mei_jun_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->mei_jun_in)) >= 8) && ((int) date('H', strtotime($absen->mei_jun_out)) < 13)
                && $absen->mei_jun_in !== 'Izin' && $absen->mei_jun_in !== 'Cuti' && $absen->mei_jun_in !== 'Sakit' && $absen->mei_jun_in !== 'UPL' && $absen->mei_jun_in !== 'Holiday' && $absen->mei_jun_in !== 'Tugas Kantor' && $absen->mei_jun_in !== 'PD Luar Negeri' && $absen->mei_jun_in !== 'Seminar / Konferensi' && $absen->mei_jun_in !== 'Pelatihan / Workshop'
                && $absen->mei_jun_out !== 'Izin' && $absen->mei_jun_out !== 'Cuti' && $absen->mei_jun_out !== 'Sakit' && $absen->mei_jun_out !== 'UPL' && $absen->mei_jun_out !== 'Holiday' && $absen->mei_jun_out !== 'Tugas Kantor' && $absen->mei_jun_out !== 'PD Luar Negeri' && $absen->mei_jun_out !== 'Seminar / Konferensi' && $absen->mei_jun_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->mei_jun_in)) >= 8) && !((int) date('H', strtotime($absen->mei_jun_out)) < 13)
                && $absen->mei_jun_in !== 'Izin' && $absen->mei_jun_in !== 'Cuti' && $absen->mei_jun_in !== 'Sakit' && $absen->mei_jun_in !== 'UPL' && $absen->mei_jun_in !== 'Holiday' && $absen->mei_jun_in !== 'Tugas Kantor' && $absen->mei_jun_in !== 'PD Luar Negeri' && $absen->mei_jun_in !== 'Seminar / Konferensi' && $absen->mei_jun_in !== 'Pelatihan / Workshop'
                && $absen->mei_jun_out !== 'Izin' && $absen->mei_jun_out !== 'Cuti' && $absen->mei_jun_out !== 'Sakit' && $absen->mei_jun_out !== 'UPL' && $absen->mei_jun_out !== 'Holiday' && $absen->mei_jun_out !== 'Tugas Kantor' && $absen->mei_jun_out !== 'PD Luar Negeri' && $absen->mei_jun_out !== 'Seminar / Konferensi' && $absen->mei_jun_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td>{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @else
                <td>{{ $absen->mei_jun_in }}</td>
                <td>{{ $absen->mei_jun_in_loc }}</td>
                <td>{{ $absen->mei_jun_out }}</td>
                <td>{{ $absen->mei_jun_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->jun_jul_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jun_jul_in }}</td>
                @elseif((int) date('i', strtotime($absen->jun_jul_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jun_jul_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->jun_jul_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '6':'7'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->jun_jul_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '6':'7'),$absen->days,$year) && $absen->jun_jul_in === '-' && $absen->jun_jul_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '6':'7').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->jun_jul_in !== 'Holiday' && $absen->jun_jul_in==$absen->jun_jul_out && ( $absen->jun_jul_in == 'Tugas Kantor' || $absen->jun_jul_in == 'PD Luar Negeri' || $absen->jun_jul_in == 'Seminar / Konferensi' || $absen->jun_jul_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->jun_jul_in }}</td>
                @elseif($absen->jun_jul_out !== '-' && $absen->jun_jul_out !== 'Holiday' && $absen->jun_jul_out !== 'Tugas Kantor' && $absen->jun_jul_out !== 'PD Luar Negeri' && $absen->jun_jul_out !== 'Seminar / Konferensi' && $absen->jun_jul_out !== 'Pelatihan / Workshop' && $absen->jun_jul_out !== 'Cuti' && $absen->jun_jul_out !== 'Sakit' && $absen->jun_jul_out !== 'Izin' && $absen->jun_jul_in==$absen->jun_jul_out && ((int) date('H', strtotime($absen->jun_jul_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @elseif($absen->jun_jul_out !== '-' && $absen->jun_jul_out !== 'Holiday' && $absen->jun_jul_out !== 'Tugas Kantor' && $absen->jun_jul_out !== 'PD Luar Negeri' && $absen->jun_jul_out !== 'Seminar / Konferensi' && $absen->jun_jul_out !== 'Pelatihan / Workshop' && $absen->jun_jul_out !== 'Cuti' && $absen->jun_jul_out !== 'Sakit' && $absen->jun_jul_out !== 'Izin' && $absen->jun_jul_in==$absen->jun_jul_out && ((int) date('H', strtotime($absen->jun_jul_in)) < 13))
                <td>{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->jun_jul_in==$absen->jun_jul_out && $absen->jun_jul_in != '-' && ($absen->jun_jul_in == 'Izin' || $absen->jun_jul_in == 'Cuti' || $absen->jun_jul_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->jun_jul_in }}</td>
                @elseif($absen->jun_jul_in==$absen->jun_jul_out && ($absen->jun_jul_in == '-' || $absen->jun_jul_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->jun_jul_in == 'Izin' || $absen->jun_jul_in == 'Cuti' || $absen->jun_jul_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @elseif($absen->jun_jul_out == 'Izin' || $absen->jun_jul_out == 'Cuti' || $absen->jun_jul_out == 'Sakit')
                <td>{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->jun_jul_out }}</td>
                @elseif(((int) date('H', strtotime($absen->jun_jul_in)) >= 8) && ((int) date('H', strtotime($absen->jun_jul_out)) < 13)
                && $absen->jun_jul_in !== 'Izin' && $absen->jun_jul_in !== 'Cuti' && $absen->jun_jul_in !== 'Sakit' && $absen->jun_jul_in !== 'UPL' && $absen->jun_jul_in !== 'Holiday' && $absen->jun_jul_in !== 'Tugas Kantor' && $absen->jun_jul_in !== 'PD Luar Negeri' && $absen->jun_jul_in !== 'Seminar / Konferensi' && $absen->jun_jul_in !== 'Pelatihan / Workshop'
                && $absen->jun_jul_out !== 'Izin' && $absen->jun_jul_out !== 'Cuti' && $absen->jun_jul_out !== 'Sakit' && $absen->jun_jul_out !== 'UPL' && $absen->jun_jul_out !== 'Holiday' && $absen->jun_jul_out !== 'Tugas Kantor' && $absen->jun_jul_out !== 'PD Luar Negeri' && $absen->jun_jul_out !== 'Seminar / Konferensi' && $absen->jun_jul_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->jun_jul_in)) >= 8) && ((int) date('H', strtotime($absen->jun_jul_out)) < 13)
                && $absen->jun_jul_in !== 'Izin' && $absen->jun_jul_in !== 'Cuti' && $absen->jun_jul_in !== 'Sakit' && $absen->jun_jul_in !== 'UPL' && $absen->jun_jul_in !== 'Holiday' && $absen->jun_jul_in !== 'Tugas Kantor' && $absen->jun_jul_in !== 'PD Luar Negeri' && $absen->jun_jul_in !== 'Seminar / Konferensi' && $absen->jun_jul_in !== 'Pelatihan / Workshop'
                && $absen->jun_jul_out !== 'Izin' && $absen->jun_jul_out !== 'Cuti' && $absen->jun_jul_out !== 'Sakit' && $absen->jun_jul_out !== 'UPL' && $absen->jun_jul_out !== 'Holiday' && $absen->jun_jul_out !== 'Tugas Kantor' && $absen->jun_jul_out !== 'PD Luar Negeri' && $absen->jun_jul_out !== 'Seminar / Konferensi' && $absen->jun_jul_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->jun_jul_in)) >= 8) && !((int) date('H', strtotime($absen->jun_jul_out)) < 13)
                && $absen->jun_jul_in !== 'Izin' && $absen->jun_jul_in !== 'Cuti' && $absen->jun_jul_in !== 'Sakit' && $absen->jun_jul_in !== 'UPL' && $absen->jun_jul_in !== 'Holiday' && $absen->jun_jul_in !== 'Tugas Kantor' && $absen->jun_jul_in !== 'PD Luar Negeri' && $absen->jun_jul_in !== 'Seminar / Konferensi' && $absen->jun_jul_in !== 'Pelatihan / Workshop'
                && $absen->jun_jul_out !== 'Izin' && $absen->jun_jul_out !== 'Cuti' && $absen->jun_jul_out !== 'Sakit' && $absen->jun_jul_out !== 'UPL' && $absen->jun_jul_out !== 'Holiday' && $absen->jun_jul_out !== 'Tugas Kantor' && $absen->jun_jul_out !== 'PD Luar Negeri' && $absen->jun_jul_out !== 'Seminar / Konferensi' && $absen->jun_jul_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td>{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @else
                <td>{{ $absen->jun_jul_in }}</td>
                <td>{{ $absen->jun_jul_in_loc }}</td>
                <td>{{ $absen->jun_jul_out }}</td>
                <td>{{ $absen->jun_jul_out_loc }}</td>
                @endif
            @endif

            @if($absen->days=='Kosong')
            <td></td>
            @elseif (in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
            <td style="text-align:left">{{ $absen->days }}</td>
            @else
            <td style="text-align:right">{{ $absen->days }}</td>
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->jul_agu_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jul_agu_in }}</td>
                @elseif((int) date('i', strtotime($absen->jul_agu_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->jul_agu_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->jul_agu_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '7':'8'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->jul_agu_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '7':'8'),$absen->days,$year) && $absen->jul_agu_in === '-' && $absen->jul_agu_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '7':'8').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->jul_agu_in !== 'Holiday' && $absen->jul_agu_in==$absen->jul_agu_out && ( $absen->jul_agu_in == 'Tugas Kantor' || $absen->jul_agu_in == 'PD Luar Negeri' || $absen->jul_agu_in == 'Seminar / Konferensi' || $absen->jul_agu_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->jul_agu_in }}</td>
                @elseif($absen->jul_agu_out !== '-' && $absen->jul_agu_out !== 'Holiday' && $absen->jul_agu_out !== 'Tugas Kantor' && $absen->jul_agu_out !== 'PD Luar Negeri' && $absen->jul_agu_out !== 'Seminar / Konferensi' && $absen->jul_agu_out !== 'Pelatihan / Workshop' && $absen->jul_agu_out !== 'Cuti' && $absen->jul_agu_out !== 'Sakit' && $absen->jul_agu_out !== 'Izin' && $absen->jul_agu_in==$absen->jul_agu_out && ((int) date('H', strtotime($absen->jul_agu_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @elseif($absen->jul_agu_out !== '-' && $absen->jul_agu_out !== 'Holiday' && $absen->jul_agu_out !== 'Tugas Kantor' && $absen->jul_agu_out !== 'PD Luar Negeri' && $absen->jul_agu_out !== 'Seminar / Konferensi' && $absen->jul_agu_out !== 'Pelatihan / Workshop' && $absen->jul_agu_out !== 'Cuti' && $absen->jul_agu_out !== 'Sakit' && $absen->jul_agu_out !== 'Izin' && $absen->jul_agu_in==$absen->jul_agu_out && ((int) date('H', strtotime($absen->jul_agu_in)) < 13))
                <td>{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->jul_agu_in==$absen->jul_agu_out && $absen->jul_agu_in != '-' && ($absen->jul_agu_in == 'Izin' || $absen->jul_agu_in == 'Cuti' || $absen->jul_agu_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->jul_agu_in }}</td>
                @elseif($absen->jul_agu_in==$absen->jul_agu_out && ($absen->jul_agu_in == '-' || $absen->jul_agu_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->jul_agu_in == 'Izin' || $absen->jul_agu_in == 'Cuti' || $absen->jul_agu_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @elseif($absen->jul_agu_out == 'Izin' || $absen->jul_agu_out == 'Cuti' || $absen->jul_agu_out == 'Sakit')
                <td>{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->jul_agu_out }}</td>
                @elseif(((int) date('H', strtotime($absen->jul_agu_in)) >= 8) && ((int) date('H', strtotime($absen->jul_agu_out)) < 13)
                && $absen->jul_agu_in !== 'Izin' && $absen->jul_agu_in !== 'Cuti' && $absen->jul_agu_in !== 'Sakit' && $absen->jul_agu_in !== 'UPL' && $absen->jul_agu_in !== 'Holiday' && $absen->jul_agu_in !== 'Tugas Kantor' && $absen->jul_agu_in !== 'PD Luar Negeri' && $absen->jul_agu_in !== 'Seminar / Konferensi' && $absen->jul_agu_in !== 'Pelatihan / Workshop'
                && $absen->jul_agu_out !== 'Izin' && $absen->jul_agu_out !== 'Cuti' && $absen->jul_agu_out !== 'Sakit' && $absen->jul_agu_out !== 'UPL' && $absen->jul_agu_out !== 'Holiday' && $absen->jul_agu_out !== 'Tugas Kantor' && $absen->jul_agu_out !== 'PD Luar Negeri' && $absen->jul_agu_out !== 'Seminar / Konferensi' && $absen->jul_agu_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->jul_agu_in)) >= 8) && ((int) date('H', strtotime($absen->jul_agu_out)) < 13)
                && $absen->jul_agu_in !== 'Izin' && $absen->jul_agu_in !== 'Cuti' && $absen->jul_agu_in !== 'Sakit' && $absen->jul_agu_in !== 'UPL' && $absen->jul_agu_in !== 'Holiday' && $absen->jul_agu_in !== 'Tugas Kantor' && $absen->jul_agu_in !== 'PD Luar Negeri' && $absen->jul_agu_in !== 'Seminar / Konferensi' && $absen->jul_agu_in !== 'Pelatihan / Workshop'
                && $absen->jul_agu_out !== 'Izin' && $absen->jul_agu_out !== 'Cuti' && $absen->jul_agu_out !== 'Sakit' && $absen->jul_agu_out !== 'UPL' && $absen->jul_agu_out !== 'Holiday' && $absen->jul_agu_out !== 'Tugas Kantor' && $absen->jul_agu_out !== 'PD Luar Negeri' && $absen->jul_agu_out !== 'Seminar / Konferensi' && $absen->jul_agu_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->jul_agu_in)) >= 8) && !((int) date('H', strtotime($absen->jul_agu_out)) < 13)
                && $absen->jul_agu_in !== 'Izin' && $absen->jul_agu_in !== 'Cuti' && $absen->jul_agu_in !== 'Sakit' && $absen->jul_agu_in !== 'UPL' && $absen->jul_agu_in !== 'Holiday' && $absen->jul_agu_in !== 'Tugas Kantor' && $absen->jul_agu_in !== 'PD Luar Negeri' && $absen->jul_agu_in !== 'Seminar / Konferensi' && $absen->jul_agu_in !== 'Pelatihan / Workshop'
                && $absen->jul_agu_out !== 'Izin' && $absen->jul_agu_out !== 'Cuti' && $absen->jul_agu_out !== 'Sakit' && $absen->jul_agu_out !== 'UPL' && $absen->jul_agu_out !== 'Holiday' && $absen->jul_agu_out !== 'Tugas Kantor' && $absen->jul_agu_out !== 'PD Luar Negeri' && $absen->jul_agu_out !== 'Seminar / Konferensi' && $absen->jul_agu_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td>{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @else
                <td>{{ $absen->jul_agu_in }}</td>
                <td>{{ $absen->jul_agu_in_loc }}</td>
                <td>{{ $absen->jul_agu_out }}</td>
                <td>{{ $absen->jul_agu_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->agu_sep_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->agu_sep_in }}</td>
                @elseif((int) date('i', strtotime($absen->agu_sep_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->agu_sep_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->agu_sep_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '8':'9'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->agu_sep_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '8':'9'),$absen->days,$year) && $absen->agu_sep_in === '-' && $absen->agu_sep_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '8':'9').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->agu_sep_in !== 'Holiday' && $absen->agu_sep_in==$absen->agu_sep_out && ( $absen->agu_sep_in == 'Tugas Kantor' || $absen->agu_sep_in == 'PD Luar Negeri' || $absen->agu_sep_in == 'Seminar / Konferensi' || $absen->agu_sep_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->agu_sep_in }}</td>
                @elseif($absen->agu_sep_out !== '-' && $absen->agu_sep_out !== 'Holiday' && $absen->agu_sep_out !== 'Tugas Kantor' && $absen->agu_sep_out !== 'PD Luar Negeri' && $absen->agu_sep_out !== 'Seminar / Konferensi' && $absen->agu_sep_out !== 'Pelatihan / Workshop' && $absen->agu_sep_out !== 'Cuti' && $absen->agu_sep_out !== 'Sakit' && $absen->agu_sep_out !== 'Izin' && $absen->agu_sep_in==$absen->agu_sep_out && ((int) date('H', strtotime($absen->agu_sep_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @elseif($absen->agu_sep_out !== '-' && $absen->agu_sep_out !== 'Holiday' && $absen->agu_sep_out !== 'Tugas Kantor' && $absen->agu_sep_out !== 'PD Luar Negeri' && $absen->agu_sep_out !== 'Seminar / Konferensi' && $absen->agu_sep_out !== 'Pelatihan / Workshop' && $absen->agu_sep_out !== 'Cuti' && $absen->agu_sep_out !== 'Sakit' && $absen->agu_sep_out !== 'Izin' && $absen->agu_sep_in==$absen->agu_sep_out && ((int) date('H', strtotime($absen->agu_sep_in)) < 13))
                <td>{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->agu_sep_in==$absen->agu_sep_out && $absen->agu_sep_in != '-' && ($absen->agu_sep_in == 'Izin' || $absen->agu_sep_in == 'Cuti' || $absen->agu_sep_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                @elseif($absen->agu_sep_in==$absen->agu_sep_out && ($absen->agu_sep_in == '-' || $absen->agu_sep_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->agu_sep_in == 'Izin' || $absen->agu_sep_in == 'Cuti' || $absen->agu_sep_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @elseif($absen->agu_sep_out == 'Izin' || $absen->agu_sep_out == 'Cuti' || $absen->agu_sep_out == 'Sakit')
                <td>{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->agu_sep_out }}</td>
                @elseif(((int) date('H', strtotime($absen->agu_sep_in)) >= 8) && ((int) date('H', strtotime($absen->agu_sep_out)) < 13)
                && $absen->agu_sep_in !== 'Izin' && $absen->agu_sep_in !== 'Cuti' && $absen->agu_sep_in !== 'Sakit' && $absen->agu_sep_in !== 'UPL' && $absen->agu_sep_in !== 'Holiday' && $absen->agu_sep_in !== 'Tugas Kantor' && $absen->agu_sep_in !== 'PD Luar Negeri' && $absen->agu_sep_in !== 'Seminar / Konferensi' && $absen->agu_sep_in !== 'Pelatihan / Workshop'
                && $absen->agu_sep_out !== 'Izin' && $absen->agu_sep_out !== 'Cuti' && $absen->agu_sep_out !== 'Sakit' && $absen->agu_sep_out !== 'UPL' && $absen->agu_sep_out !== 'Holiday' && $absen->agu_sep_out !== 'Tugas Kantor' && $absen->agu_sep_out !== 'PD Luar Negeri' && $absen->agu_sep_out !== 'Seminar / Konferensi' && $absen->agu_sep_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->agu_sep_in)) >= 8) && ((int) date('H', strtotime($absen->agu_sep_out)) < 13)
                && $absen->agu_sep_in !== 'Izin' && $absen->agu_sep_in !== 'Cuti' && $absen->agu_sep_in !== 'Sakit' && $absen->agu_sep_in !== 'UPL' && $absen->agu_sep_in !== 'Holiday' && $absen->agu_sep_in !== 'Tugas Kantor' && $absen->agu_sep_in !== 'PD Luar Negeri' && $absen->agu_sep_in !== 'Seminar / Konferensi' && $absen->agu_sep_in !== 'Pelatihan / Workshop'
                && $absen->agu_sep_out !== 'Izin' && $absen->agu_sep_out !== 'Cuti' && $absen->agu_sep_out !== 'Sakit' && $absen->agu_sep_out !== 'UPL' && $absen->agu_sep_out !== 'Holiday' && $absen->agu_sep_out !== 'Tugas Kantor' && $absen->agu_sep_out !== 'PD Luar Negeri' && $absen->agu_sep_out !== 'Seminar / Konferensi' && $absen->agu_sep_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->agu_sep_in)) >= 8) && !((int) date('H', strtotime($absen->agu_sep_out)) < 13)
                && $absen->agu_sep_in !== 'Izin' && $absen->agu_sep_in !== 'Cuti' && $absen->agu_sep_in !== 'Sakit' && $absen->agu_sep_in !== 'UPL' && $absen->agu_sep_in !== 'Holiday' && $absen->agu_sep_in !== 'Tugas Kantor' && $absen->agu_sep_in !== 'PD Luar Negeri' && $absen->agu_sep_in !== 'Seminar / Konferensi' && $absen->agu_sep_in !== 'Pelatihan / Workshop'
                && $absen->agu_sep_out !== 'Izin' && $absen->agu_sep_out !== 'Cuti' && $absen->agu_sep_out !== 'Sakit' && $absen->agu_sep_out !== 'UPL' && $absen->agu_sep_out !== 'Holiday' && $absen->agu_sep_out !== 'Tugas Kantor' && $absen->agu_sep_out !== 'PD Luar Negeri' && $absen->agu_sep_out !== 'Seminar / Konferensi' && $absen->agu_sep_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td>{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @else
                <td>{{ $absen->agu_sep_in }}</td>
                <td>{{ $absen->agu_sep_in_loc }}</td>
                <td>{{ $absen->agu_sep_out }}</td>
                <td>{{ $absen->agu_sep_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->sep_okt_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->sep_okt_in }}</td>
                @elseif((int) date('i', strtotime($absen->sep_okt_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->sep_okt_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->sep_okt_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '9':'10'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->sep_okt_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '9':'10'),$absen->days,$year) && $absen->sep_okt_in === '-' && $absen->sep_okt_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '9':'10').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->sep_okt_in !== 'Holiday' && $absen->sep_okt_in==$absen->sep_okt_out && ( $absen->sep_okt_in == 'Tugas Kantor' || $absen->sep_okt_in == 'PD Luar Negeri' || $absen->sep_okt_in == 'Seminar / Konferensi' || $absen->sep_okt_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->sep_okt_in }}</td>
                @elseif($absen->sep_okt_out !== '-' && $absen->sep_okt_out !== 'Holiday' && $absen->sep_okt_out !== 'Tugas Kantor' && $absen->sep_okt_out !== 'PD Luar Negeri' && $absen->sep_okt_out !== 'Seminar / Konferensi' && $absen->sep_okt_out !== 'Pelatihan / Workshop' && $absen->sep_okt_out !== 'Cuti' && $absen->sep_okt_out !== 'Sakit' && $absen->sep_okt_out !== 'Izin' && $absen->sep_okt_in==$absen->sep_okt_out && ((int) date('H', strtotime($absen->sep_okt_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @elseif($absen->sep_okt_out !== '-' && $absen->sep_okt_out !== 'Holiday' && $absen->sep_okt_out !== 'Tugas Kantor' && $absen->sep_okt_out !== 'PD Luar Negeri' && $absen->sep_okt_out !== 'Seminar / Konferensi' && $absen->sep_okt_out !== 'Pelatihan / Workshop' && $absen->sep_okt_out !== 'Cuti' && $absen->sep_okt_out !== 'Sakit' && $absen->sep_okt_out !== 'Izin' && $absen->sep_okt_in==$absen->sep_okt_out && ((int) date('H', strtotime($absen->sep_okt_in)) < 13))
                <td>{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->sep_okt_in==$absen->sep_okt_out && $absen->sep_okt_in != '-' && ($absen->sep_okt_in == 'Izin' || $absen->sep_okt_in == 'Cuti' || $absen->sep_okt_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->sep_okt_in }}</td>
                @elseif($absen->sep_okt_in==$absen->sep_okt_out && ($absen->sep_okt_in == '-' || $absen->sep_okt_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->sep_okt_in == 'Izin' || $absen->sep_okt_in == 'Cuti' || $absen->sep_okt_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @elseif($absen->sep_okt_out == 'Izin' || $absen->sep_okt_out == 'Cuti' || $absen->sep_okt_out == 'Sakit')
                <td>{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->sep_okt_out }}</td>
                @elseif(((int) date('H', strtotime($absen->sep_okt_in)) >= 8) && ((int) date('H', strtotime($absen->sep_okt_out)) < 13)
                && $absen->sep_okt_in !== 'Izin' && $absen->sep_okt_in !== 'Cuti' && $absen->sep_okt_in !== 'Sakit' && $absen->sep_okt_in !== 'UPL' && $absen->sep_okt_in !== 'Holiday' && $absen->sep_okt_in !== 'Tugas Kantor' && $absen->sep_okt_in !== 'PD Luar Negeri' && $absen->sep_okt_in !== 'Seminar / Konferensi' && $absen->sep_okt_in !== 'Pelatihan / Workshop'
                && $absen->sep_okt_out !== 'Izin' && $absen->sep_okt_out !== 'Cuti' && $absen->sep_okt_out !== 'Sakit' && $absen->sep_okt_out !== 'UPL' && $absen->sep_okt_out !== 'Holiday' && $absen->sep_okt_out !== 'Tugas Kantor' && $absen->sep_okt_out !== 'PD Luar Negeri' && $absen->sep_okt_out !== 'Seminar / Konferensi' && $absen->sep_okt_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->sep_okt_in)) >= 8) && ((int) date('H', strtotime($absen->sep_okt_out)) < 13)
                && $absen->sep_okt_in !== 'Izin' && $absen->sep_okt_in !== 'Cuti' && $absen->sep_okt_in !== 'Sakit' && $absen->sep_okt_in !== 'UPL' && $absen->sep_okt_in !== 'Holiday' && $absen->sep_okt_in !== 'Tugas Kantor' && $absen->sep_okt_in !== 'PD Luar Negeri' && $absen->sep_okt_in !== 'Seminar / Konferensi' && $absen->sep_okt_in !== 'Pelatihan / Workshop'
                && $absen->sep_okt_out !== 'Izin' && $absen->sep_okt_out !== 'Cuti' && $absen->sep_okt_out !== 'Sakit' && $absen->sep_okt_out !== 'UPL' && $absen->sep_okt_out !== 'Holiday' && $absen->sep_okt_out !== 'Tugas Kantor' && $absen->sep_okt_out !== 'PD Luar Negeri' && $absen->sep_okt_out !== 'Seminar / Konferensi' && $absen->sep_okt_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->sep_okt_in)) >= 8) && !((int) date('H', strtotime($absen->sep_okt_out)) < 13)
                && $absen->sep_okt_in !== 'Izin' && $absen->sep_okt_in !== 'Cuti' && $absen->sep_okt_in !== 'Sakit' && $absen->sep_okt_in !== 'UPL' && $absen->sep_okt_in !== 'Holiday' && $absen->sep_okt_in !== 'Tugas Kantor' && $absen->sep_okt_in !== 'PD Luar Negeri' && $absen->sep_okt_in !== 'Seminar / Konferensi' && $absen->sep_okt_in !== 'Pelatihan / Workshop'
                && $absen->sep_okt_out !== 'Izin' && $absen->sep_okt_out !== 'Cuti' && $absen->sep_okt_out !== 'Sakit' && $absen->sep_okt_out !== 'UPL' && $absen->sep_okt_out !== 'Holiday' && $absen->sep_okt_out !== 'Tugas Kantor' && $absen->sep_okt_out !== 'PD Luar Negeri' && $absen->sep_okt_out !== 'Seminar / Konferensi' && $absen->sep_okt_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td>{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @else
                <td>{{ $absen->sep_okt_in }}</td>
                <td>{{ $absen->sep_okt_in_loc }}</td>
                <td>{{ $absen->sep_okt_out }}</td>
                <td>{{ $absen->sep_okt_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->okt_nov_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->okt_nov_in }}</td>
                @elseif((int) date('i', strtotime($absen->okt_nov_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->okt_nov_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->okt_nov_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '10':'11'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->okt_nov_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '10':'11'),$absen->days,$year) && $absen->okt_nov_in === '-' && $absen->okt_nov_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '10':'11').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->okt_nov_in !== 'Holiday' && $absen->okt_nov_in==$absen->okt_nov_out && ( $absen->okt_nov_in == 'Tugas Kantor' || $absen->okt_nov_in == 'PD Luar Negeri' || $absen->okt_nov_in == 'Seminar / Konferensi' || $absen->okt_nov_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->okt_nov_in }}</td>
                @elseif($absen->okt_nov_out !== '-' && $absen->okt_nov_out !== 'Holiday' && $absen->okt_nov_out !== 'Tugas Kantor' && $absen->okt_nov_out !== 'PD Luar Negeri' && $absen->okt_nov_out !== 'Seminar / Konferensi' && $absen->okt_nov_out !== 'Pelatihan / Workshop' && $absen->okt_nov_out !== 'Cuti' && $absen->okt_nov_out !== 'Sakit' && $absen->okt_nov_out !== 'Izin' && $absen->okt_nov_in==$absen->okt_nov_out && ((int) date('H', strtotime($absen->okt_nov_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @elseif($absen->okt_nov_out !== '-' && $absen->okt_nov_out !== 'Holiday' && $absen->okt_nov_out !== 'Tugas Kantor' && $absen->okt_nov_out !== 'PD Luar Negeri' && $absen->okt_nov_out !== 'Seminar / Konferensi' && $absen->okt_nov_out !== 'Pelatihan / Workshop' && $absen->okt_nov_out !== 'Cuti' && $absen->okt_nov_out !== 'Sakit' && $absen->okt_nov_out !== 'Izin' && $absen->okt_nov_in==$absen->okt_nov_out && ((int) date('H', strtotime($absen->okt_nov_in)) < 13))
                <td>{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->okt_nov_in==$absen->okt_nov_out && $absen->okt_nov_in != '-' && ($absen->okt_nov_in == 'Izin' || $absen->okt_nov_in == 'Cuti' || $absen->okt_nov_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->okt_nov_in }}</td>
                @elseif($absen->okt_nov_in==$absen->okt_nov_out && ($absen->okt_nov_in == '-' || $absen->okt_nov_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->okt_nov_in == 'Izin' || $absen->okt_nov_in == 'Cuti' || $absen->okt_nov_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @elseif($absen->okt_nov_out == 'Izin' || $absen->okt_nov_out == 'Cuti' || $absen->okt_nov_out == 'Sakit')
                <td>{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->okt_nov_out }}</td>
                @elseif(((int) date('H', strtotime($absen->okt_nov_in)) >= 8) && ((int) date('H', strtotime($absen->okt_nov_out)) < 13)
                && $absen->okt_nov_in !== 'Izin' && $absen->okt_nov_in !== 'Cuti' && $absen->okt_nov_in !== 'Sakit' && $absen->okt_nov_in !== 'UPL' && $absen->okt_nov_in !== 'Holiday' && $absen->okt_nov_in !== 'Tugas Kantor' && $absen->okt_nov_in !== 'PD Luar Negeri' && $absen->okt_nov_in !== 'Seminar / Konferensi' && $absen->okt_nov_in !== 'Pelatihan / Workshop'
                && $absen->okt_nov_out !== 'Izin' && $absen->okt_nov_out !== 'Cuti' && $absen->okt_nov_out !== 'Sakit' && $absen->okt_nov_out !== 'UPL' && $absen->okt_nov_out !== 'Holiday' && $absen->okt_nov_out !== 'Tugas Kantor' && $absen->okt_nov_out !== 'PD Luar Negeri' && $absen->okt_nov_out !== 'Seminar / Konferensi' && $absen->okt_nov_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->okt_nov_in)) >= 8) && ((int) date('H', strtotime($absen->okt_nov_out)) < 13)
                && $absen->okt_nov_in !== 'Izin' && $absen->okt_nov_in !== 'Cuti' && $absen->okt_nov_in !== 'Sakit' && $absen->okt_nov_in !== 'UPL' && $absen->okt_nov_in !== 'Holiday' && $absen->okt_nov_in !== 'Tugas Kantor' && $absen->okt_nov_in !== 'PD Luar Negeri' && $absen->okt_nov_in !== 'Seminar / Konferensi' && $absen->okt_nov_in !== 'Pelatihan / Workshop'
                && $absen->okt_nov_out !== 'Izin' && $absen->okt_nov_out !== 'Cuti' && $absen->okt_nov_out !== 'Sakit' && $absen->okt_nov_out !== 'UPL' && $absen->okt_nov_out !== 'Holiday' && $absen->okt_nov_out !== 'Tugas Kantor' && $absen->okt_nov_out !== 'PD Luar Negeri' && $absen->okt_nov_out !== 'Seminar / Konferensi' && $absen->okt_nov_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->okt_nov_in)) >= 8) && !((int) date('H', strtotime($absen->okt_nov_out)) < 13)
                && $absen->okt_nov_in !== 'Izin' && $absen->okt_nov_in !== 'Cuti' && $absen->okt_nov_in !== 'Sakit' && $absen->okt_nov_in !== 'UPL' && $absen->okt_nov_in !== 'Holiday' && $absen->okt_nov_in !== 'Tugas Kantor' && $absen->okt_nov_in !== 'PD Luar Negeri' && $absen->okt_nov_in !== 'Seminar / Konferensi' && $absen->okt_nov_in !== 'Pelatihan / Workshop'
                && $absen->okt_nov_out !== 'Izin' && $absen->okt_nov_out !== 'Cuti' && $absen->okt_nov_out !== 'Sakit' && $absen->okt_nov_out !== 'UPL' && $absen->okt_nov_out !== 'Holiday' && $absen->okt_nov_out !== 'Tugas Kantor' && $absen->okt_nov_out !== 'PD Luar Negeri' && $absen->okt_nov_out !== 'Seminar / Konferensi' && $absen->okt_nov_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td>{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @else
                <td>{{ $absen->okt_nov_in }}</td>
                <td>{{ $absen->okt_nov_in_loc }}</td>
                <td>{{ $absen->okt_nov_out }}</td>
                <td>{{ $absen->okt_nov_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->nov_des_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->nov_des_in }}</td>
                @elseif((int) date('i', strtotime($absen->nov_des_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->nov_des_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->nov_des_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '11':'12'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->nov_des_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '11':'12'),$absen->days,$year) && $absen->nov_des_in === '-' && $absen->nov_des_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '11':'12').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->nov_des_in !== 'Holiday' && $absen->nov_des_in==$absen->nov_des_out && ( $absen->nov_des_in == 'Tugas Kantor' || $absen->nov_des_in == 'PD Luar Negeri' || $absen->nov_des_in == 'Seminar / Konferensi' || $absen->nov_des_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->nov_des_in }}</td>
                @elseif($absen->nov_des_out !== '-' && $absen->nov_des_out !== 'Holiday' && $absen->nov_des_out !== 'Tugas Kantor' && $absen->nov_des_out !== 'PD Luar Negeri' && $absen->nov_des_out !== 'Seminar / Konferensi' && $absen->nov_des_out !== 'Pelatihan / Workshop' && $absen->nov_des_out !== 'Cuti' && $absen->nov_des_out !== 'Sakit' && $absen->nov_des_out !== 'Izin' && $absen->nov_des_in==$absen->nov_des_out && ((int) date('H', strtotime($absen->nov_des_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @elseif($absen->nov_des_out !== '-' && $absen->nov_des_out !== 'Holiday' && $absen->nov_des_out !== 'Tugas Kantor' && $absen->nov_des_out !== 'PD Luar Negeri' && $absen->nov_des_out !== 'Seminar / Konferensi' && $absen->nov_des_out !== 'Pelatihan / Workshop' && $absen->nov_des_out !== 'Cuti' && $absen->nov_des_out !== 'Sakit' && $absen->nov_des_out !== 'Izin' && $absen->nov_des_in==$absen->nov_des_out && ((int) date('H', strtotime($absen->nov_des_in)) < 13))
                <td>{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->nov_des_in==$absen->nov_des_out && $absen->nov_des_in != '-' && ($absen->nov_des_in == 'Izin' || $absen->nov_des_in == 'Cuti' || $absen->nov_des_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->nov_des_in }}</td>
                @elseif($absen->nov_des_in==$absen->nov_des_out && ($absen->nov_des_in == '-' || $absen->nov_des_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->nov_des_in == 'Izin' || $absen->nov_des_in == 'Cuti' || $absen->nov_des_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @elseif($absen->nov_des_out == 'Izin' || $absen->nov_des_out == 'Cuti' || $absen->nov_des_out == 'Sakit')
                <td>{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->nov_des_out }}</td>
                @elseif(((int) date('H', strtotime($absen->nov_des_in)) >= 8) && ((int) date('H', strtotime($absen->nov_des_out)) < 13)
                && $absen->nov_des_in !== 'Izin' && $absen->nov_des_in !== 'Cuti' && $absen->nov_des_in !== 'Sakit' && $absen->nov_des_in !== 'UPL' && $absen->nov_des_in !== 'Holiday' && $absen->nov_des_in !== 'Tugas Kantor' && $absen->nov_des_in !== 'PD Luar Negeri' && $absen->nov_des_in !== 'Seminar / Konferensi' && $absen->nov_des_in !== 'Pelatihan / Workshop'
                && $absen->nov_des_out !== 'Izin' && $absen->nov_des_out !== 'Cuti' && $absen->nov_des_out !== 'Sakit' && $absen->nov_des_out !== 'UPL' && $absen->nov_des_out !== 'Holiday' && $absen->nov_des_out !== 'Tugas Kantor' && $absen->nov_des_out !== 'PD Luar Negeri' && $absen->nov_des_out !== 'Seminar / Konferensi' && $absen->nov_des_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->nov_des_in)) >= 8) && ((int) date('H', strtotime($absen->nov_des_out)) < 13)
                && $absen->nov_des_in !== 'Izin' && $absen->nov_des_in !== 'Cuti' && $absen->nov_des_in !== 'Sakit' && $absen->nov_des_in !== 'UPL' && $absen->nov_des_in !== 'Holiday' && $absen->nov_des_in !== 'Tugas Kantor' && $absen->nov_des_in !== 'PD Luar Negeri' && $absen->nov_des_in !== 'Seminar / Konferensi' && $absen->nov_des_in !== 'Pelatihan / Workshop'
                && $absen->nov_des_out !== 'Izin' && $absen->nov_des_out !== 'Cuti' && $absen->nov_des_out !== 'Sakit' && $absen->nov_des_out !== 'UPL' && $absen->nov_des_out !== 'Holiday' && $absen->nov_des_out !== 'Tugas Kantor' && $absen->nov_des_out !== 'PD Luar Negeri' && $absen->nov_des_out !== 'Seminar / Konferensi' && $absen->nov_des_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->nov_des_in)) >= 8) && !((int) date('H', strtotime($absen->nov_des_out)) < 13)
                && $absen->nov_des_in !== 'Izin' && $absen->nov_des_in !== 'Cuti' && $absen->nov_des_in !== 'Sakit' && $absen->nov_des_in !== 'UPL' && $absen->nov_des_in !== 'Holiday' && $absen->nov_des_in !== 'Tugas Kantor' && $absen->nov_des_in !== 'PD Luar Negeri' && $absen->nov_des_in !== 'Seminar / Konferensi' && $absen->nov_des_in !== 'Pelatihan / Workshop'
                && $absen->nov_des_out !== 'Izin' && $absen->nov_des_out !== 'Cuti' && $absen->nov_des_out !== 'Sakit' && $absen->nov_des_out !== 'UPL' && $absen->nov_des_out !== 'Holiday' && $absen->nov_des_out !== 'Tugas Kantor' && $absen->nov_des_out !== 'PD Luar Negeri' && $absen->nov_des_out !== 'Seminar / Konferensi' && $absen->nov_des_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td>{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @else
                <td>{{ $absen->nov_des_in }}</td>
                <td>{{ $absen->nov_des_in_loc }}</td>
                <td>{{ $absen->nov_des_out }}</td>
                <td>{{ $absen->nov_des_out_loc }}</td>
                @endif
            @endif

            @if(in_array($absen->days,['Absen','Ijin','PA','PA (h:mm)','PL','PL (h:mm)','TD','TD (h:mm)','DTT','PTT','Cuti','Cuti Khusus','UPL','Sakit','Pjl.Dinas','Sem./konf.','Plth./wkshp.','Jml.hr.krj.','Hdr.hr.krj.','Kosong','Jml.hr.lib.']))
                @if($absen->des_jan_in > 0 && ($absen->days == 'Absen' || $absen->days == 'DTT' || $absen->days == 'PTT'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->des_jan_in }}</td>
                @elseif((int) date('i', strtotime($absen->des_jan_in)) > 4 && ($absen->days == 'PA (h:mm)' || $absen->days == 'PL (h:mm)' || $absen->days == 'TD (h:mm)'))
                <td colspan="4" style="background-color: #830a04;text-align:right">{{ $absen->des_jan_in }}</td>
                @else
                <td colspan="4" style="text-align:right">{{ $absen->des_jan_in }}</td>
                @endif
            @else
                @if(!checkdate(($absen->first_month == 1 ?  '12':'1'),$absen->days,$year))
                <td colspan="4" style="background-color: #000000"></td>
                @elseif($absen->des_jan_in !== 'Holiday' && checkdate(($absen->first_month == 1 ?  '12':'1'),$absen->days,$year) && $absen->des_jan_in === '-' && $absen->des_jan_out === '-' && date('N', strtotime($year.'-'.($absen->first_month == 1 ?  '12':'1').'-'.$absen->days)) < 6)
                <td colspan="4" style="background-color: #6818e1">{{ 'Absen' }}</td>
                @elseif($absen->des_jan_in !== 'Holiday' && $absen->des_jan_in==$absen->des_jan_out && ( $absen->des_jan_in == 'Tugas Kantor' || $absen->des_jan_in == 'PD Luar Negeri' || $absen->des_jan_in == 'Seminar / Konferensi' || $absen->des_jan_in == 'Pelatihan / Workshop'))
                <td colspan="4" >{{ $absen->des_jan_in }}</td>
                @elseif($absen->des_jan_out !== '-' && $absen->des_jan_out !== 'Holiday' && $absen->des_jan_out !== 'Tugas Kantor' && $absen->des_jan_out !== 'PD Luar Negeri' && $absen->des_jan_out !== 'Seminar / Konferensi' && $absen->des_jan_out !== 'Pelatihan / Workshop' && $absen->des_jan_out !== 'Cuti' && $absen->des_jan_out !== 'Sakit' && $absen->des_jan_out !== 'Izin' && $absen->des_jan_in==$absen->des_jan_out && ((int) date('H', strtotime($absen->des_jan_in)) >= 13))
                <td colspan="2" style="background-color: #e118c6">{{ 'DTT' }}</td>
                <td>{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @elseif($absen->des_jan_out !== '-' && $absen->des_jan_out !== 'Holiday' && $absen->des_jan_out !== 'Tugas Kantor' && $absen->des_jan_out !== 'PD Luar Negeri' && $absen->des_jan_out !== 'Seminar / Konferensi' && $absen->des_jan_out !== 'Pelatihan / Workshop' && $absen->des_jan_out !== 'Cuti' && $absen->des_jan_out !== 'Sakit' && $absen->des_jan_out !== 'Izin' && $absen->des_jan_in==$absen->des_jan_out && ((int) date('H', strtotime($absen->des_jan_in)) < 13))
                <td>{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td colspan="2" style="background-color: #e118c6" >{{ 'PTT' }}</td>
                @elseif($absen->des_jan_in==$absen->des_jan_out && $absen->des_jan_in != '-' && ($absen->des_jan_in == 'Izin' || $absen->des_jan_in == 'Cuti' || $absen->des_jan_in == 'Sakit'))
                <td colspan="4" style="background-color: #ea6e0d">{{ $absen->des_jan_in }}</td>
                @elseif($absen->des_jan_in==$absen->des_jan_out && ($absen->des_jan_in == '-' || $absen->des_jan_in == 'Holiday'))
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                <td colspan="2" style="background-color: #64abff">{{ '-' }}</td>
                @elseif($absen->des_jan_in == 'Izin' || $absen->des_jan_in == 'Cuti' || $absen->des_jan_in == 'Sakit')
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @elseif($absen->des_jan_out == 'Izin' || $absen->des_jan_out == 'Cuti' || $absen->des_jan_out == 'Sakit')
                <td>{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td colspan="2" style="background-color: #ea6e0d">{{ $absen->des_jan_out }}</td>
                @elseif(((int) date('H', strtotime($absen->des_jan_in)) >= 8) && ((int) date('H', strtotime($absen->des_jan_out)) < 13)
                && $absen->des_jan_in !== 'Izin' && $absen->des_jan_in !== 'Cuti' && $absen->des_jan_in !== 'Sakit' && $absen->des_jan_in !== 'UPL' && $absen->des_jan_in !== 'Holiday' && $absen->des_jan_in !== 'Tugas Kantor' && $absen->des_jan_in !== 'PD Luar Negeri' && $absen->des_jan_in !== 'Seminar / Konferensi' && $absen->des_jan_in !== 'Pelatihan / Workshop'
                && $absen->des_jan_out !== 'Izin' && $absen->des_jan_out !== 'Cuti' && $absen->des_jan_out !== 'Sakit' && $absen->des_jan_out !== 'UPL' && $absen->des_jan_out !== 'Holiday' && $absen->des_jan_out !== 'Tugas Kantor' && $absen->des_jan_out !== 'PD Luar Negeri' && $absen->des_jan_out !== 'Seminar / Konferensi' && $absen->des_jan_out !== 'Pelatihan / Workshop')
                <td style="background-color: #ff2c23">{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @elseif(!((int) date('H', strtotime($absen->des_jan_in)) >= 8) && ((int) date('H', strtotime($absen->des_jan_out)) < 13)
                && $absen->des_jan_in !== 'Izin' && $absen->des_jan_in !== 'Cuti' && $absen->des_jan_in !== 'Sakit' && $absen->des_jan_in !== 'UPL' && $absen->des_jan_in !== 'Holiday' && $absen->des_jan_in !== 'Tugas Kantor' && $absen->des_jan_in !== 'PD Luar Negeri' && $absen->des_jan_in !== 'Seminar / Konferensi' && $absen->des_jan_in !== 'Pelatihan / Workshop'
                && $absen->des_jan_out !== 'Izin' && $absen->des_jan_out !== 'Cuti' && $absen->des_jan_out !== 'Sakit' && $absen->des_jan_out !== 'UPL' && $absen->des_jan_out !== 'Holiday' && $absen->des_jan_out !== 'Tugas Kantor' && $absen->des_jan_out !== 'PD Luar Negeri' && $absen->des_jan_out !== 'Seminar / Konferensi' && $absen->des_jan_out !== 'Pelatihan / Workshop')
                <td>{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td style="background-color: #ff2c23">{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @elseif(((int) date('H', strtotime($absen->des_jan_in)) >= 8) && !((int) date('H', strtotime($absen->des_jan_out)) < 13)
                && $absen->des_jan_in !== 'Izin' && $absen->des_jan_in !== 'Cuti' && $absen->des_jan_in !== 'Sakit' && $absen->des_jan_in !== 'UPL' && $absen->des_jan_in !== 'Holiday' && $absen->des_jan_in !== 'Tugas Kantor' && $absen->des_jan_in !== 'PD Luar Negeri' && $absen->des_jan_in !== 'Seminar / Konferensi' && $absen->des_jan_in !== 'Pelatihan / Workshop'
                && $absen->des_jan_out !== 'Izin' && $absen->des_jan_out !== 'Cuti' && $absen->des_jan_out !== 'Sakit' && $absen->des_jan_out !== 'UPL' && $absen->des_jan_out !== 'Holiday' && $absen->des_jan_out !== 'Tugas Kantor' && $absen->des_jan_out !== 'PD Luar Negeri' && $absen->des_jan_out !== 'Seminar / Konferensi' && $absen->des_jan_out !== 'Pelatihan / Workshop' )
                <td style="background-color: #ff2c23">{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td>{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @else
                <td>{{ $absen->des_jan_in }}</td>
                <td>{{ $absen->des_jan_in_loc }}</td>
                <td>{{ $absen->des_jan_out }}</td>
                <td>{{ $absen->des_jan_out_loc }}</td>
                @endif
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
