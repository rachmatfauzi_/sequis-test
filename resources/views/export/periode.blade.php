<table border="100">
    <thead>
    <tr></tr>
    <tr>
        <th colspan="18" style="text-align: left"><strong>Presensi Karyawan PMBS, Periode: {{$dateStart}} s.d. {{$dateEnd}}</strong></th>
        <th colspan="15" style="text-align: left"><strong>Presensi Karyawan PMBS, Periode: {{$dateStart}} s.d. {{$dateEnd}}</strong></th>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            NIP
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Name
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Homebase
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Ttl. Tlmb.Dtg. > 08:00
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Ttl. Tlmb.Dtg. 08:01 - 08:15
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Ttl. Tlmb.Dtg. 08:16 - 08:30
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Ttl. Tlmb.Dtg. > 08:30
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Pulang Awal &lt; 17:00
        </th>
        <th style="background-color: #808080;text-align:center" colspan="2">
            Pulang Larut >= 17:30
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Absen/Tnp. Ket.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Dtg.Tdk.Tcatat.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Plg.Tdk.Tcatat.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            NIP
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Name
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Homebase
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Cuti
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Cuti Khusus
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            UPL
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Sakit
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Ijin
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Pjl. Dinas
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Sem./Konf.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Plth./Wkshp
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Jml. Hr. Krj.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Hadir Hr. Krj.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            T. Hadir Hr. Krj.
        </th>
        <th style="background-color: #808080;text-align:center" rowspan="2">
            Jml. Hr. Lib
        </th>
    </tr>
    <tr>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
        <th style="background-color: #808080;text-align:center" >frek.</th>
        <th style="background-color: #808080;text-align:center" >jam</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $absen)
        <tr>
            <td>{{ $absen->pernr }}</td>
            <td>{{ $absen->sname }}</td>
            <td>{{ $absen->homebase }}</td>
            <td>{{ $absen->total_terlambat_datang }}</td>
            @if(((int)explode(':', $absen->total_waktu_terlambat_datang)[0] * 3600 + (int)explode(':', $absen->total_waktu_terlambat_datang)[1] * 60 + (int)explode(':', $absen->total_waktu_terlambat_datang)[2]) > 180)
            <td style="background-color: #ea0d0d">{{ explode(':', $absen->total_waktu_terlambat_datang)[0].':'.explode(':', $absen->total_waktu_terlambat_datang)[1].':'.explode(':', $absen->total_waktu_terlambat_datang)[2] }}</td>
            @else
            <td>{{ explode(':', $absen->total_waktu_terlambat_datang)[0].':'.explode(':', $absen->total_waktu_terlambat_datang)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang)[2])[0] }}</td>
            @endif
            <td>{{ explode(':', $absen->total_waktu_terlambat_datang)[0].':'.explode(':', $absen->total_waktu_terlambat_datang)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang)[2])[0]  }}</td>
            @if(((int)explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[0] * 3600 + (int)explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[1] * 60 + (int)explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[2]) > 180)
            <td style="background-color: #ea0d0d">{{ explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[2])[0] }}</td>
            @else
            <td>{{ explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0801_0815)[2])[0] }}</td>
            @endif
            <td>{{ $absen->total_terlambat_datang_0816_0830 }}</td>
            @if(((int)explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[0] * 3600 + (int)explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[1] * 60 + (int)explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[2]) > 180)
            <td style="background-color: #ea0d0d">{{ explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[2])[0] }}</td>
            @else
            <td>{{ explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0816_0830)[2])[0] }}</td>
            @endif
            <td>{{ $absen->total_terlambat_datang_0830 }}</td>
            @if(((int)explode(':', $absen->total_waktu_terlambat_datang_0830)[0] * 3600 + (int)explode(':', $absen->total_waktu_terlambat_datang_0830)[1] * 60 + (int)explode(':', $absen->total_waktu_terlambat_datang_0830)[2]) > 180)
            <td style="background-color: #ea0d0d">{{ explode(':', $absen->total_waktu_terlambat_datang_0830)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0830)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0830)[2])[0]  }}</td>
            @else
            <td>{{ explode(':', $absen->total_waktu_terlambat_datang_0830)[0].':'.explode(':', $absen->total_waktu_terlambat_datang_0830)[1].':'.explode('.',explode(':', $absen->total_waktu_terlambat_datang_0830)[2])[0] }}</td>
            @endif
            <td>{{ $absen->total_pulang_awal }}</td>
            <td>{{ $absen->total_waktu_pulang_awal }}</td>
            <td>{{ $absen->total_pulang_larut }}</td>
            @if(((int)explode(':', $absen->total_waktu_pulang_larut)[0] * 3600 + (int)explode(':', $absen->total_waktu_pulang_larut)[1] * 60 + (int)explode(':', $absen->total_waktu_pulang_larut)[2]) > 180)
            <td style="background-color: #ea0d0d">{{ explode(':', $absen->total_waktu_pulang_larut)[0].':'.explode(':', $absen->total_waktu_pulang_larut)[1].':'.explode('.',explode(':', $absen->total_waktu_pulang_larut)[2])[0] }}</td>
            @else
            <td>{{ explode(':', $absen->total_waktu_pulang_larut)[0].':'.explode(':', $absen->total_waktu_pulang_larut)[1].':'.explode('.',explode(':', $absen->total_waktu_pulang_larut)[2])[0]}}</td>
            @endif
            @if($absen->total_absen > 0)
            <td style="background-color: #ea0d0d">{{ $absen->total_absen }}</td>
            @else
            <td>{{ $absen->total_absen }}</td>
            @endif
            @if($absen->total_datang_tidak_tercatat > 0)
            <td style="background-color: #ea0d0d">{{ $absen->total_datang_tidak_tercatat }}</td>
            @else
            <td>0</td>
            @endif
            @if($absen->total_pulang_tidak_tercatat > 0)
            <td style="background-color: #ea0d0d">{{ $absen->total_pulang_tidak_tercatat }}</td>
            @else
            <td>0</td>
            @endif
            <td>{{ $absen->pernr }}</td>
            <td>{{ $absen->sname }}</td>
            <td>{{ $absen->homebase }}</td>
            <td>{{ $absen->total_cuti > 0 ? $absen->total_cuti : 0 }}</td>
            <td>{{ $absen->total_cuti_khusus > 0 ? $absen->total_cuti_khusus : 0 }}</td>
            @if($absen->total_upl > 0)
            <td style="background-color: #ea0d0d">{{ $absen->total_upl }}</td>
            @else
            <td>0</td>
            @endif
            <td>{{ $absen->total_sakit > 0 ? $absen->total_sakit : 0 }}</td>
            <td>{{ $absen->total_ijin > 0 ? $absen->total_ijin : 0 }}</td>
            <td>{{ $absen->total_pd_luar_negeri }}</td>
            <td>{{ $absen->total_seminar }}</td>
            <td>{{ $absen->total_pelatihan }}</td>
            <td>{{ $absen->jumlah_hari_kerja }}</td>
            <td>{{ $absen->total_hari_bekerja_pada_hari_kerja }}</td>
            <td>{{ $absen->total_tidak_hadir_hari_kerja }}</td>
            <td>{{ $absen->jumlah_hari_libur }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
