<div class=""><div class="aHl"></div><div id=":ph" tabindex="-1"></div><div id=":p6" class="ii gt" jslog="20277; u014N:xr6bB; 4:W251bGwsbnVsbCxbXV0."><div id=":p5" class="a3s aiL "><u></u>
            <div style="height:100%!important;width:100%!important;background-color:#f4f5f9;margin:0;padding:0">
                <table border="0" cellpadding="0" cellspacing="0" align="center" style="font-size:13px;color:#39394d;font-family:Arial;background-color:#f4f5f9;width:100%;height:100%;padding-bottom:10px">
                    <tbody><tr>
                        <td style="vertical-align:top">
                            <table id="m_-1757472886975548857table-new-registration" border="0" cellpadding="0" cellspacing="0" align="center" style="width:592px">
                                <tbody><tr>
                                    <td style="vertical-align:top;padding:0px;background-color:#f4f5f9">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center" style="margin-left:0px;margin-right:0px;width:100%;background-color:#ffffff;padding-bottom:37px">
                                            <tbody><tr>
                                                <td style="padding-top:30px;padding-bottom:10px;background-color:#f4f5f9;text-align:center">
                                                    <h2> Work Permit Request </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="m_-1757472886975548857td1" style="padding-left:28px;padding-right:28px">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" style="width:100%;font-size:13px;color:#39394d;font-family:Arial">

                                                        <tbody><tr>
                                                            <td style="height:40px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-size:13px;color:#39394d;font-family:Arial;vertical-align:top;line-height:20px;padding-top:10px">Hello Mr/Mrs, {{$data['email_recipient_name']}}</td>
                                                        </tr>

                                                        <tr style="height:12px"></tr>

                                                        <tr>
                                                            <td colspan="2" style="font-size:13px;color:#39394d;font-family:Arial;vertical-align:top;line-height:20px">
                                                                Your request has been {{$data['status_desc']}}.
                                                            </td>
                                                        </tr>

                                                        <tr style="height:16px"></tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <table style="width:100%;border-radius:10px;background-color:#f7f7fc">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td colspan="3" style="padding-left:16px;vertical-align:top;padding-top:18px"><strong>Request Detail :</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">Name</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['requester_name']}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">Initial</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['requester_initial']}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">ID</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['requester_id']}}</td>
                                                                    </tr>
                                                                    <tr><td colspan="2"></td></tr>

                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">Start Date</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['form_start_date']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">End Date</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['form_end_date']}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">Leave Type</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['form_type']}}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">Reason</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['form_reason']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">{{$data['status']}} By</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['responder_name']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-left:16px;padding-top:16px;height:18px;width:22%;vertical-align:top;color:#747487">{{$data['status']}} Time</td>
                                                                        <td style="vertical-align:top;padding-top:16px">{{$data['responder_time']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height:16px;width:22%"></td>
                                                                        <td></td>
                                                                    </tr>

                                                                    </tbody></table>
                                                        <tr>
                                                            <td style="height:16px;width:22%"></td>
                                                            <td></td>
                                                        </tr>
                                                        </td>

                                                        </tr>

                                                        <tr></tr>

<!--                                                        <tr>-->
<!--                                                            <td colspan="2" style="font-family:Arial;font-size:16px;font-weight:600;line-height:25px;color:#39394d;padding-top:12px">-->
<!--                                                                To keep this meeting secure, do not share this link publicly.-->
<!--                                                            </td>-->
<!--                                                        </tr>-->

                                                        </tbody></table>
                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" style="width:100%;font-size:13px;color:#39394d;font-family:Arial">
                                                        <tbody><tr>
                                                            <td style="padding-top:28px;padding-bottom:28px">
                                                                Thank you!
                                                            </td>
                                                        </tr>
                                                        </tbody></table>
                                                </td>
                                            </tr>

                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                    </tr>
                    </tbody></table><div class="yj6qo"></div><div class="adL">
                </div></div><div class="adL">

            </div></div></div><div id=":pl" class="ii gt" style="display:none"><div id=":pm" class="a3s aiL "></div></div><div class="hi"></div></div>
