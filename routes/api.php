<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\FriendController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'login'], function () {
    Route::post('/', [AuthController::class, 'login']);
});

// add ['middleware' => ['api', 'cors'], 'prefix' => 'permit'] for auth
Route::group(['prefix' => 'friend'], function () {
    Route::post('/common', [FriendController::class, 'fetchFriendInCommon']);
    Route::get('/request', [FriendController::class, 'fetchFriendRequest']);
    Route::get('/request/{id}', [FriendController::class, 'fetchFriendList']);
    Route::post('/request/', [FriendController::class, 'postFriendRequest']);
    Route::put('/request/{status}', [FriendController::class, 'updateFriendRequest']);
    Route::put('/block/{id}', [FriendController::class, 'postBlockRequest']);
});


