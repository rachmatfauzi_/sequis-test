<?php

namespace App\Services;

use App\Models\MasterRole;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserServices
{
    // index
    public function fetchAll($req)
    {
        $search = $req['search'] ?? null;
        unset($req['search']);
        $query = User::where($req);

        if(isset($search)){
            $query->whereRaw("full_name LIKE ?",['%'.$search.'%']);
        }
        return $query->get();

    }

    // show user by id
    public function fetchById($id)
    {
        return User::whereId($id)->first();
    }

    // show user by fullname
    public function fetchByFullname($fullname)
    {
        return User::where('full_name', $fullname)->first();
    }

    // show user by email
    public function fetchByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    // store user
    public function store($data)
    {
        try {
            $user = User::create([
                'full_name' => $data->full_name,
                'password' => md5($data->password),
                'email' => $data->email,
                'employee_id' => $data->employee_id,
            ]);

            return $user;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // update user
    public function update($user, $data)
    {
        try {
            $user->full_name = $data->full_name;

            if ($data->new_password != null && $data->new_password != '')
                $user->password = md5($data->new_password);

            $user->email = $data->email;
            $user->employee_id = $data->employee_id;

            $user->save();

            return $user;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function checkPassword($id, $password) {
        $user = User::with('role.master_role')->whereId($id)->where('password', md5($password))->first();

        return $user;
    }

    public function updatePassword($user, $data) {
        try {
            $user->update([
                'password' => md5($data->new_password)
            ]);

            return $user;
        } catch (\Throwable $e) {
            throw $e;
        }
    }

}
