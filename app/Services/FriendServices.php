<?php

namespace App\Services;

use App\Models\BlockList;
use App\Models\Friend;
use App\Models\FriendRequest;
use App\Models\Status;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FriendServices
{
    public function fetchAllFriendRequest($req)
    {
        $email = $req['email'] ?? null;

        $query = DB::table('friend_request')->select(
            DB::raw('requestor.email as requestor'),
            DB::raw('status.description as status'))
            ->leftJoin('users as requestor','requestor.id','=','friend_request.requester_id')
            ->leftJoin('users as receiver','receiver.id','=','friend_request.receiver_id')
            ->leftJoin('status','status.id','=','friend_request.status')
            ->where('receiver.email',$email);

        return $query->orderBy('requestor.email', 'desc')->get();
    }

    public function fetchAllFriendInCommon($request)
    {

        $query = DB::table('friends')->select(
            DB::raw('receiver.email as friends'))
            ->leftJoin('users as requestor','requestor.id','=','friend.user_id')
            ->leftJoin('users as receiver','receiver.id','=','friend_request.friend_user_id')
            ->whereIn('id', $request->friends);

        return $query->orderBy('requestor.email', 'desc')->get();
    }

    public function fetchAllFriendList($req)
    {
        $email = $req['email'] ?? null;

        $query = DB::table('friend')->select(
            DB::raw('receiver.email as friends'))
            ->leftJoin('users as requestor','requestor.id','=','friend.user_id')
            ->leftJoin('users as receiver','receiver.id','=','friend_request.friend_user_id')
            ->where('receiver.email',$email);

        return $query->orderBy('receiver.email', 'desc')->get();
    }

    public function createFriendRequest($data)
    {
        try {
            DB::beginTransaction();

            $params = [
                'requester_id' => $data->requester_id,
                'receiver_id' => $data->receiver_id,
                'status' => 1,
            ];

            $request = FriendRequest::create($params);

            if ($request) {
                DB::commit();
                return $request;
            }

            DB::rollback();
            return $request;
        } catch (Exception $e) {
            DB::rollback();
            return $request;
        }
    }

    public function createBlockRequest($data)
    {
        try {
            DB::beginTransaction();

            $params = [
                'user_id' => $data->requestor,
                'block_user_id' => $data->block,
            ];

            $request = BlockList::create($params);

            if ($request) {
                DB::commit();
                return $request;
            }

            DB::rollback();
            return $request;
        } catch (Exception $e) {
            DB::rollback();
            return $request;
        }
    }

    public function updateFriendRequest($data)
    {
        try {
            DB::beginTransaction();

            $reqParam = [
                'status' => $data->status,
            ];

            $request = FriendRequest::where('id', $data->id)
                ->update($reqParam);
            if($request){
                DB::commit();
                return false;
            }

            DB::rollBack();
            return false;
        } catch (Exception $e) {
            DB::rollBack();
            return false;
        }
    }

    // kehadiran master
    public function fetchKetidakhadiran($id)
    {
        return MasterKetidakhadiran::where('subty', $id)->first();
    }

    // cek kehadiran block
    public function checkBlock($data)
    {
        return BlockList::where([
            'block_user_id' => $data->block_user_id,
            'user_id' => $data->user_id,
        ])->first();
    }

    public function checkRequest($data)
    {
        return WorkPermit::where([
//            'subty' => $data->subty,
            'pernr' => $data->pernr,
        ])->where(function($query) use ($data) {
            $query->where(function ($query) use ($data) {
                $query->whereRaw("DATE(start_date) <= ?",[date('Y-m-d',strtotime($data->start_date))])
                    ->whereRaw("DATE(end_date) >= ?",[date('Y-m-d',strtotime($data->start_date))]);
            })->orWhere(function ($query) use ($data) {
                $query->whereRaw("DATE(start_date) <= ?",[date('Y-m-d',strtotime($data->end_date))])
                    ->whereRaw("DATE(end_date) >= ?",[date('Y-m-d',strtotime($data->end_date))]);
            })->orWhere(function ($query) use ($data) {
                $query->whereRaw("DATE(start_date) >= ?",[date('Y-m-d',strtotime($data->start_date))])
                    ->whereRaw("DATE(end_date) >= ?",[date('Y-m-d',strtotime($data->start_date))])
                    ->whereRaw("DATE(start_date) <= ?",[date('Y-m-d',strtotime($data->end_date))])
                    ->whereRaw("DATE(end_date) <= ?",[date('Y-m-d',strtotime($data->end_date))]);
            });
        })->first();
    }

    public function fetchListMasterNotif($id)
    {
        return NotificationGroupSendList::select(DB::raw('DISTINCT users.email'))
            ->leftJoin('notification_group_mapping', 'notification_group_mapping.group_id', '=', 'notification_group_send_list.group_id')
            ->leftJoin('users', 'users.sap_id', '=', 'notification_group_mapping.user_id')
            ->where('notification_id', $id)->get();

    }

    public function updateQuota($data)
    {
        try {
            DB::beginTransaction();
            $checkData = DB::table('pa2006')->select('anzhl')
                ->where(['sk', $data->sk, 'pernr' => $data->pernr])->get();

            $presensi = DB::table('pa2006')
                ->where(['sk', $data->sk, 'pernr' => $data->pernr])
                ->update([
                    'anzhl' => $checkData[0]->anzhl - $data->total_days,
                ]);
            if($presensi) {
                DB::commit();
                return $presensi;
            }
            DB::rollBack();
            return $presensi;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateDataQuota($data)
    {
        try {
            DB::beginTransaction();
            $checkData = DB::table('pa2006')->select('is_suspend','subty','deend')
                ->whereRaw("sk = ?",[$data->sk])->get();
            $totalMonthAdding = 3;
            if($checkData[0]->subty != '1000'){
                $totalMonthAdding = 6;
            }
            $presensi = DB::table('pa2006')
                ->whereRaw("sk = ?",[$data->sk])
                ->update([
                    'is_suspend' => $checkData[0]->is_suspend == 0 ? 1 : 0,
                    'deend' => $checkData[0]->is_suspend == 0 ? date('Y-m-d', strtotime("+".$totalMonthAdding." months", strtotime($checkData[0]->deend))) : date('Y-m-d', strtotime("-".$totalMonthAdding." months", strtotime($checkData[0]->deend))),
                ]);
            if($presensi) {
                DB::commit();
                return $presensi;
            }
            DB::rollBack();
            return $presensi;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    // all ketidakhadiran master
    public function fetchAllKetidakhadiran($req)
    {
        return MasterKetidakhadiran::where($req)->get();
    }

    public function batchInsertQuota($data)
    {
        return LeaveQuota::insert($data);
    }

    public function batchPostData($dataKetidakhadiran,$dataRequest)
    {
        try {
            DB::beginTransaction();
            $result = Ketidakhadiran::insert($dataKetidakhadiran);
            $resultPermit = WorkPermit::insert($dataRequest);

            if(isset($result) && isset($resultPermit)){
                DB::commit();
                return $result;
            }
            DB::rollBack();
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    public function fetchLeaveQuota($req)
    {
//        $dateFrom = $req['desta'] ?? null;
//        $dateTo = $req['deend'] ?? null;
//        unset($req['desta']);
//        unset($req['deend']);
        $search = $req['search'] ?? null;
        $period = $req['period'] ?? null;

        $query = DB::table('pa2006')
            ->select('mk.leave_name','mk.total_days','pa2006.is_suspend','user.cname as fullname','user.rufnm as initial','pa2006.pernr','pa2006.sk','pa2006.subty','pa2006.begda','pa2006.endda','pa2006.desta','pa2006.deend',
                DB::raw('mk.total_days-pa2006.anzhl as total_quota_used'),
                DB::raw('mk.total_days as total_quota_all'),
                DB::raw('YEAR(pa2006.deend) as period'),
                DB::raw('pa2006.anzhl as total_quota'))
            ->leftJoin('pa0002 as user','user.pernr','=','pa2006.pernr')
            ->leftJoin('master_ketidakhadiran as mk','mk.subty','=','pa2006.subty');

        if(isset($req['pa2006.desta'])){
            $query->where(function($query) use ($req) {
                $query->where(function ($query) use ($req) {
                    $query->whereRaw("? between `desta` AND `deend`",[$req['pa2006.desta']]);
                });
            });
        }
        if(isset($search)){
            $query->whereRaw("user.cname LIKE ?",['%'.$search.'%']);
        }
        if(isset($period)){
            $query->whereRaw("YEAR(pa2006.deend) = ?",[$period]);
        }

        unset($req['pa2006.desta']);
        unset($req['pa2006.deend']);
        unset($req['search']);
        unset($req['period']);
        return $query->where($req)->get();
    }

    public function fetchUserDataQuota($req)
    {
//        $dateFrom = $req['da'] ?? null;
        $query = DB::select("select join_master.dat01,
STR_TO_DATE(CONCAT(YEAR(NOW()),'-',month(CONCAT(YEAR(NOW()),'-',MONTH(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01))),'-',DAY(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01))))),'-',day(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)))),'%Y-%m-%d') as a_begda,

STR_TO_DATE(CONCAT(YEAR(NOW())+1,'-',month(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01))),'-',DAY(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)))), INTERVAL 1 DAY)),'-',day(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)), INTERVAL 1 DAY))),'%Y-%m-%d') as a_endda,

STR_TO_DATE(CONCAT(YEAR(NOW()),'-',month(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)), INTERVAL 1 MONTH)),'-',day(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)), INTERVAL 1 MONTH))),'%Y-%m-%d') as a_desta,

STR_TO_DATE(CONCAT(YEAR(NOW()+1),'-',month(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)), INTERVAL 1 DAY)),'-',day(DATE_SUB(CONCAT(YEAR(NOW()),'-',MONTH(join_master.dat01),'-',DAY(join_master.dat01)), INTERVAL 1 DAY))),'%Y-%m-%d') as a_deend,

join_master.pernr,cuti.cuti_tahunan,cuti.cuti_5tahunan, year(now())-year(join_master.dat01) as year_ctr from
(select pa0041.* from pa0041 INNER JOIN
(select max(endda) as endda, pernr from pa0041 group by pernr) jdate on pa0041.pernr = jdate.pernr and pa0041.endda = jdate.endda ) join_master
LEFT JOIN
(select CASE WHEN pa0001.ansvh IN ('Z1','Z2','Z5') and pa0001.persg IN ('1','2') THEN 1 ELSE 0 END as cuti_tahunan,
CASE WHEN pa0001.ansvh IN ('Z1') and pa0001.persg IN ('1','2') THEN 1 ELSE 0 END as cuti_5tahunan,
pa0001.pernr from pa0001 inner join (
select max(begda) as begda, pernr from pa0001 group by pernr) amin on pa0001.pernr = amin.pernr and pa0001.begda = amin.begda ) cuti on join_master.pernr = cuti.pernr
WHERE STR_TO_DATE(CONCAT(YEAR(NOW()),'-',month(DATE_SUB(join_master.dat01, INTERVAL 1 MONTH)),'-',day(join_master.dat01)),'%Y-%m-%d') = DATE(NOW())");

        return $query;
    }

}
