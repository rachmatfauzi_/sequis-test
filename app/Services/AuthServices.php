<?php

namespace App\Services;

use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;

class AuthServices
{
    public function fetchGoogleAccount($token) 
        {    
            try {
        
                $google_user = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $token)[1]))));

                if ($google_user)
                    return $google_user;

                return false;
                
            } catch (Exception $e) {
                throw $e;
            }
        }

    public function generateJWTToken($data) 
        {    
            try {
                $token = JWTAuth::fromUser($data);

                if ($token)
                    return ['user' => $data, 'access_token' => $token];

                return false;
                
            } catch (Exception $e) {
                throw $e;
            }
        }
}