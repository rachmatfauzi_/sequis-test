<?php

namespace App\Http\Traits;

use Illuminate\Http\Response;

trait ApiResponses
{
    public function success($statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'success' => true,
        ], $statusCode);
    }

    public function successRes($data, $message, $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message
        ], $statusCode);
    }

    public function friendRequestRes($data, $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'requests' => $data,
        ], $statusCode);
    }

    public function friendInCommonRes($data, $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'success' => true,
            'friends' => $data,
            'count' => count($data),
        ], $statusCode);
    }

    public function friendListRes($data, $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            'friends' => $data,
        ], $statusCode);
    }

    public function errorRes($message = 'Bad Request', $statusCode = 400)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
        ], $statusCode);
    }
}
