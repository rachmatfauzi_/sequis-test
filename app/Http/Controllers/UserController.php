<?php

namespace App\Http\Controllers;

use App\Services\UserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $userServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserServices $userServices)
        {
            $this->middleware('auth:api');
            $this->userServices = $userServices;
        }

    // Get all user data
    public function index(Request $request)
    {
        $param = [];
        if(isset($request->all()['search']))
            $param['search'] = $request->all()['search'];
        if(isset($request->all()['id']))
            $param['id'] = $request->all()['id'];
        if(isset($request->all()['full_name']))
            $param['full_name'] = $request->all()['full_name'];
        if(isset($request->all()['sap_id']))
            $param['sap_id'] = $request->all()['sap_id'];
        if(isset($request->all()['initial']))
            $param['initial'] = $request->all()['initial'];

        $users = $this->userServices->fetchAll($param);

        if ($users)
            return $this->successRes($users, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Users'), 404);

    }

    // Get all user data
    public function getRole(Request $request)
    {
        $param = [];

        $users = $this->userServices->getAllRole($param);

        if ($users)
            return $this->successRes($users, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Users'), 404);

    }

    public function updateRole(Request $request)
    {
        $roles = isset($request->all()['role_list']) ? $request->all()['role_list'] : [];
        $id = $request->all()['id'];
        $arrayNew = [];
        foreach ($roles as $role) {
            $arrayNew[] = [
                'user_id' => $request->all()['id'],
                'master_role_id' => $role,
            ];
        }

        $store = $this->userServices->updateRole($arrayNew,$id);

        if($store){
            return $this->successRes($store, msgStored());
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    // Get one user data by ID
    public function show($id)
    {
        $user = $this->userServices->fetchById($id);

        if (!$user)
            return $this->errorRes(msgNotFound('User'), 404);

        return $this->successRes($user, msgFetch(), 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150',
            'alias' => 'nullable|string|min:3|max:3',
            'email' => 'required|email|max:50',
            'username' => 'required|string|max:40',
            'password' => 'required|string|max:120',
            'phone_number' => 'required|string|max:13',
            'active' => 'required|integer',
            'signature' => 'nullable|mimes:jpeg,jpg,png|max:1000'
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }
        try {

            // Check for similar username
            $user = $this->userServices->fetchByFullname($request->name);

            if ($user)
                return $this->errorRes(msgFound('Similar user'), 404);

            // Store User
            $store = $this->userServices->store($request);

            if($store){
                return $this->successRes($store, msgStored());
            }else{
                return $this->errorRes(msgNotStored());
            }
        } catch(\Exception $e){
            return $this->errorRes($e);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'full_name' => 'required|string|max:40',
            'password' => 'nullable|string|max:120',
            'email' => 'required|email|max:50',
            'sap_id' => 'nullable|string|max:50',
            'initial' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }
        try {

            if ($request->new_password != $request->reenter_new_password)
                return $this->errorRes('New Passwords are not match !', 403);

            // Check if user exist
            $user = $this->userServices->fetchById($request->id);

            if (!$user)
                return $this->errorRes(msgNotFound('User'), 404);

            // Check Password
            $check_password = $this->userServices->checkPassword($user->id, $request->old_password);

            if (!$check_password)
                return $this->errorRes(msgWrongPassword(), 403);

            // Update
            $update = $this->userServices->update($user, $request);

            if($update){
                return $this->successRes($update, msgUpdated());
            }else{
                return $this->errorRes(msgNotUpdated());
            }
        } catch(\Exception $e){
            return $this->errorRes($e);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
//            'id' => 'required|integer',
            'full_name' => 'required|string|max:40',
            'password' => 'nullable|string|max:120',
            'reenter_password' => 'nullable|string|max:120',
            'email' => 'required|email|max:50',
            'sap_id' => 'nullable|string|max:50',
            'initial' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }
        try {

            if ($request->password != $request->reenter_password)
                return $this->errorRes('Passwords are not match !', 403);

            // Check if user exist
            $user = $this->userServices->fetchBySAPid($request->sap_id);

            if ($user)
                return $this->errorRes('User is already exist', 400);

            // Create
            $create = $this->userServices->store($request);

            if($create){
                return $this->successRes($create, msgStored());
            }else{
                return $this->errorRes(msgNotStored());
            }
        } catch(\Exception $e){
            return $this->errorRes($e);
        }
    }

    public function checkPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $user = $this->userServices->checkPassword($request);

        if (!$user)
            return $this->errorRes('Password incorrect', 400);

        return $this->successRes(true, 'Password correct', 200);
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'password' => 'required',
            'new_password' => 'required|string|max:120',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $user = $this->userServices->checkPassword($request);

        if (!$user)
            return $this->errorRes(msgNotFound('User'), 400);

        try {
            $user = $this->userServices->updatePassword($user, $request);
        } catch (\Throwable $e) {
            return $this->errorRes($e->getMessage(), 500);
        }

        return $this->successRes($user, msgUpdated(), 200);
    }
}
