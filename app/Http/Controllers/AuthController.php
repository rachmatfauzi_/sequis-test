<?php

namespace App\Http\Controllers;

use App\Services\AuthServices;
use App\Services\UserServices;
use App\Services\PersonalDataServices;
use App\Services\CommunicationServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use stdClass;

class AuthController extends Controller
{
    private $authServices, $userServices;

    public function __construct(AuthServices $authServices, UserServices $userServices)
    {
        $this->authServices = $authServices;
        $this->userServices = $userServices;
    }

    public function login(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'username' => 'required|string|max:40',
                'password' => 'required|string|max:120',
            ]);

            if ($validator->fails())
                return $this->errorRes($validator->getMessageBag()->toArray());

            $user = $this->userServices->fetchByEmail($request->username);


            if (!$user)
                return $this->errorRes(msgNotFound('Users'), 404);

            $check_password = $this->userServices->checkPassword($user->id, $request->password);

            if (!$check_password)
                return $this->errorRes(msgWrongPassword(), 403);

            $data = $this->authServices->generateJWTToken($user);

            if ($data)
                return $this->successRes($data, msgFetch(), 200);

            return $this->errorRes(msgNotFound('Users'), 404);

        }
}
