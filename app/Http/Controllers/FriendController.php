<?php

namespace App\Http\Controllers;

use App\Services\FriendServices;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DateTime;

class FriendController extends Controller
{
    private $friendServices;
    private $approvalTokenServices;
    private $userServices;
    private $workPermitServices;
    private $organizationalAssignmentServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FriendServices $friendServices)
    {
        $this->friendServices = $friendServices;
    }

    public function fetchFriendRequest(Request $request)
    {
        $param = [];
        if(isset($request->all()['email']))
            $param['email'] = $request->all()['email'];

        $request = $this->friendServices->fetchAllFriendRequest($param);

        if (isset($request))
            return $this->friendRequestRes($request, 200);

        return $this->errorRes(msgNotFound('Permit Request'), 404);

    }

    public function fetchFriendInCommon(Request $request)
    {

        $request = $this->friendServices->fetchAllFriendInCommon($request);

        if (isset($request))
            return $this->friendInCommonRes($request, 200);

        return $this->errorRes(msgNotFound(), 404);

    }

    public function fetchFriendList(Request $request)
    {
        $param = [];
        if(isset($request->all()['email']))
            $param['email'] = $request->all()['email'];

        $request = $this->friendServices->fetchAllFriendList($param);

        if (isset($request))
            return $this->friendListRes($request, 200);

        return $this->errorRes(msgNotFound('Permit Request'), 404);

    }

    public function postFriendRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'requester_id' => 'required|integer',
            'receiver_id' => 'required|string|max:20',
            'status' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        // check whether the user has a history request that has been approved before
        $request = $this->friendServices->checkBlock($request);

        if(isset($request)){
            return $this->errorRes('Cannot send friend request');
        }

        $request = $this->friendServices->createFriendRequest($request);

        if(isset($request)){
            return $this->errorRes('Request Failed');
        }

        if($request){
            return $this->success(200);
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    public function updateFriendRequest(Request $request, $status)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'requester_id' => 'required|integer',
            'receiver_id' => 'required|string|max:20',
            'status' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $request = $this->friendServices->updateFriendRequest($request);

        if($request){
            return $this->success();
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    public function postBlockRequest(Request $request, $status)
    {
        $validator = Validator::make($request->all(), [
            'requestor' => 'required|integer',
            'block' => 'required|string|max:20',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $request = $this->friendServices->createBlockRequest($request);

        if($request){
            return $this->success();
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    public function showById($id)
    {

        $leaveServices = $this->leaveServices->fetchById($id);

        if ($leaveServices)
            return $this->successRes($leaveServices, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Leave Type'), 404);

    }

    public function checkToken($token)
    {

        $tokenServices = $this->approvalTokenServices->fetchById($token);

        if ($tokenServices)
            return $this->successRes($tokenServices, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Leave Type'), 404);

    }

    public function showBySAPid($id)
    {

        $leaveServices = $this->leaveServices->fetchBySAPid($id);

        if ($leaveServices)
            return $this->successRes($leaveServices, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Leave Type'), 404);

    }

    public function postDataRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pernr' => 'required|integer',
//            'start_date' => 'date_format:"Y-m-d H:i:s"|after_or_equal:' . date(DATE_ATOM),
//            'end_date' => 'date_format:"Y-m-d H:i:s"|after_or_equal:' . date(DATE_ATOM),
            'subty' => 'required|string|max:20',
            'note' => 'required|string|max:225',
            'status' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        // check whether the user has a history request that has been approved before
        $checkKetidakhadiranRecord = $this->workPermitServices->checkKetidakhadiran($request);

        if(isset($checkKetidakhadiranRecord)){
            return $this->errorRes('You have leave or permit record between or in these dates');
        }

        $checkKetidakhadiranRequest = $this->workPermitServices->checkRequest($request);

        if(isset($checkKetidakhadiranRequest)){
            return $this->errorRes('You have leave or permit request between or in these dates, please check your request status');
        }

        // check approver
        $approver = $this->organizationalAssignmentServices->getApprover($request->all()['pernr']);
        $masterKetidakhadiran = $this->workPermitServices->fetchKetidakhadiran($request->all()['subty']);
        $request->request->add(['atype_desc' => $masterKetidakhadiran->leave_name]);
        $request->request->add(['notification_type' => 2]);
        if ($approver->statusApproval || isset($request->all()['requester_pernr'])) {
            // SDM user or boss
            $request->request->add(['status' => 1]);
            $request->request->add(['approved_time' => date("Y-m-d H:i:s")]);
            $request->request->add(['approved_sdm_time' => date("Y-m-d H:i:s")]);
            $request->request->add(['approved_by_id' => isset($request->all()['requester_pernr']) ? $request->all()['requester_pernr'] : $request->all()['pernr']]);
            $request->request->add(['approved_by_sdm_id' => isset($request->all()['requester_pernr']) ? $request->all()['requester_pernr'] : $request->all()['pernr']]);
            $request->request->add(['ket' => null]);
            $request->request->add(['is_sdm' => isset($request->all()['requester_pernr']) ? true : $approver->statusApproval]);
        } else {
            if($request->all()['is_sdm_approver'] == 0 ){
                // di approve oleh atasan
                $request->request->add(['approved_by_id' => $approver->approver]);
            } else {
                // sakit di approve oleh SDM
                $request->all()['approved_by_id'] = null;
            }

            $request->request->add(['ket' => null]);
            $request->request->add(['is_sdm' => false]);
        }
        $store = $this->workPermitServices->postDataRequest($request);
        $userRequest = $this->userServices->fetchBySAPid($request->all()['pernr']);
        if(isset($userRequest)) {
            $request->request->add(['requester_name' => $userRequest->full_name]);
            $request->request->add(['requester_id' => $userRequest->sap_id]);
            $request->request->add(['requester_initial' => $userRequest->initial]);
            $request->request->add(['requester_email' => $userRequest->email]);
        }
        $userApprover = $this->userServices->fetchBySAPid($approver->approver);
        $sdmEmail = $this->userServices->fetchByRoleName('SDM');
        $notifFromMasterNotif = $this->workPermitServices->fetchListMasterNotif($request->all()['pernr']);

        $arrayEmail = [];
        foreach ($notifFromMasterNotif as $index => $key){
            $arrayEmail[] = $key->email;
        }

        $sdmEmailList = [];
        foreach($sdmEmail as $user){
            $sdmEmailList[] = $user->email;
        }
        $arrayMasterNotif = $arrayEmail;

        if(isset($notifFromMasterNotif) && count($notifFromMasterNotif) > 0) {
            for($i=0;$i<count($arrayMasterNotif);$i++){
                $sdmEmailList[] = $arrayMasterNotif[$i];
            }
        }
        $request->request->add(['sdm_email' => $sdmEmailList]);

        if(isset($userApprover)) {
            $request->request->add(['superior_name' => $userApprover->full_name]);
            $request->request->add(['superior_id' => $userApprover->sap_id]);
            $request->request->add(['superior_initial' => $userApprover->initial]);
            $request->request->add(['superior_email' => $userApprover->email]);
        }

        if($store['status']){
            $request->request->add(['token' => $store['token']]);
            if($approver->statusApproval)
                return $this->successRes($store['data'], msgStored());
            if($this->sendMailApprovalLeave($request))
                return $this->successRes($store['data'], msgStored());
            else
                return $this->successRes($store['data'], 'Data has been stored, but failed to send email');
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    public function postDataRequestBackup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pernr' => 'required|integer',
//            'start_date' => 'date_format:"Y-m-d H:i:s"|after_or_equal:' . date(DATE_ATOM),
//            'end_date' => 'date_format:"Y-m-d H:i:s"|after_or_equal:' . date(DATE_ATOM),
            'subty' => 'required|string|max:20',
            'note' => 'required|string|max:225',
            'status' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        // check approver
        $approver = $this->organizationalAssignmentServices->getApprover($request->all()['pernr']);
        $masterKetidakhadiran = $this->workPermitServices->fetchKetidakhadiran($request->all()['subty']);
        $request->request->add(['atype_desc' => $masterKetidakhadiran->leave_name]);
        if ($approver->statusApproval) {
            // SDM user or boss
            $request->request->add(['status' => 1]);
            $request->request->add(['approved_time' => date("Y-m-d H:i:s")]);
            $request->request->add(['approved_sdm_time' => date("Y-m-d H:i:s")]);
            $request->request->add(['approved_by_id' => $request->all()['pernr']]);
            $request->request->add(['approved_by_sdm_id' => $request->all()['pernr']]);
            $request->request->add(['ket' => null]);
            $request->request->add(['notification_type' => 6]);
            $request->request->add(['is_sdm' => $approver->statusApproval]);
        } else {
            // di approve oleh atasan
            $request->request->add(['approved_by_id' => $approver->approver]);
            // sakit di approve oleh SDM
            if($request->all()['type_ketidakhadiran'] == 2){
                $request->all()['approved_by_id'] = null;
            }

            $request->request->add(['notification_type' => 6]);
            $request->request->add(['ket' => null]);
            $request->request->add(['is_sdm' => false]);
        }

        $store = $this->workPermitServices->postDataRequest($request);
        $userRequest = $this->userServices->fetchBySAPid($request->all()['pernr']);
        if(isset($userRequest)) {
            $request->request->add(['requester_name' => $userRequest->full_name]);
            $request->request->add(['requester_id' => $userRequest->sap_id]);
            $request->request->add(['requester_initial' => $userRequest->initial]);
            $request->request->add(['requester_email' => $userRequest->email]);
        }
        $userApprover = $this->userServices->fetchBySAPid($approver->approver);
        $sdmEmail = $this->userServices->fetchByRoleName('SDM');
        $sdmEmailList = [];
        foreach($sdmEmail as $user){
            $sdmEmailList[] = $user->email;
        }
        $request->request->add(['sdm_email' => $sdmEmailList]);

        if(isset($userApprover)) {
            $request->request->add(['superior_name' => $userApprover->full_name]);
            $request->request->add(['superior_id' => $userApprover->sap_id]);
            $request->request->add(['superior_initial' => $userApprover->initial]);
            $request->request->add(['superior_email' => $userApprover->email]);
        }

        if($store['status']){
            $request->request->add(['token' => $store['token']]);
            if($approver->statusApproval)
                return $this->successRes($store['data'], msgStored());
            if($this->sendMailApprovalLeave($request))
                return $this->successRes($store['data'], msgStored());
            else
                return $this->successRes($store['data'], 'Data has been stored, but failed to send email');
        }else{
            return $this->errorRes(msgNotStored());
        }
    }

    public function updateDataRequest(Request $request)
    {
        $validatorParam = [
            'requestor' => 'required|integer',
            'approved_by_id' => 'required|integer',
            'pernr' => 'required|integer',
        ];
        $validator = Validator::make($request->all(), $validatorParam);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $request = $this->friendServices->updateFriendRequest($request);

        if($request){
            return $this->successRes(msgUpdated());
        } else{
            return $this->errorRes(msgNotUpdated());
        }
    }

    public function updateDataQuota(Request $request)
    {
        $validatorParam = [
            'sk' => 'required|integer',
        ];
        $validator = Validator::make($request->all(), $validatorParam);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $store = $this->workPermitServices->updateDataQuota($request);

        if($store){
             return $this->successRes($store, msgUpdated());
        }else{
            return $this->errorRes(msgNotUpdated());
        }
    }

    public function updateDataRequestBackup(Request $request)
    {
        $validatorParam = [];
        if($request->all()['status'] == 1){
            $validatorParam = [
                'sk' => 'required|integer',
                'approved_by_id' => 'required|integer',
                'pernr' => 'required|integer',
            ];
        }
        if($request->all()['status'] == 2){
            $validatorParam = [
                'sk' => 'required|integer',
                'canceled_by_id' => 'required|integer',
                'pernr' => 'required|integer',
            ];
        }
        $validator = Validator::make($request->all(), $validatorParam);

        if ($validator->fails()) {
            return $this->errorRes($validator->getMessageBag()->toArray());
        }

        $store = $this->workPermitServices->updateDataRequest($request);

        if($store){
            $userRequest = $this->userServices->fetchBySAPid($request->all()['pernr']);

            if(isset($userRequest)) {
                $request->request->add(['requester_name' => $userRequest->full_name]);
                $request->request->add(['requester_id' => $userRequest->sap_id]);
                $request->request->add(['requester_initial' => $userRequest->initial]);
                $request->request->add(['requester_email' => $userRequest->email]);
            }
            $userApprover = $this->userServices->fetchBySAPid($request->all()['status'] == 2 ? $request->all()['canceled_by_id'] : $request->all()['approved_by_id']);
            if(isset($userApprover)) {
                $request->request->add(['responder_name' => $userApprover->full_name]);
                $request->request->add(['responder_id' => $userApprover->sap_id]);
                $request->request->add(['responder_initial' => $userApprover->initial]);
                $request->request->add(['responder_email' => $userApprover->email]);
            }
            $formData = $this->workPermitServices->fetchById($request->all()['sk']);
            $masterKetidakhadiran = $this->workPermitServices->fetchKetidakhadiran($request->all()['subty']);
            $request->request->add(['atype_desc' => $masterKetidakhadiran->leave_name]);
            $request->request->add(['status' => $formData->status ?:$request->all()['status']]);
            $request->request->add(['reason' => $formData->note]);
            $request->request->add(['start_date' => $formData->start_date]);
            $request->request->add(['end_date' => $formData->end_date]);
            $request->request->add(['isfullday' => $formData->isfullday]);
            $request->request->add(['event' => $formData->event]);
            $request->request->add(['total_days' => $formData->total_days]);
            $request->request->add(['approved_time' => $formData->approved_time]);
            $request->request->add(['approved_sdm_time' => $formData->approved_sdm_time]);
            $request->request->add(['canceled_time' => $formData->canceled_time]);

            $sdmEmail = $this->userServices->fetchByRoleName('SDM');
            $sdmEmailList = [];
            foreach($sdmEmail as $user){
                $sdmEmailList[] = $user->email;
            }
            $request->request->add(['sdm_email' => $sdmEmailList]);

            if($request->all()['is_send_notif'] == 1) {
                if ($this->sendMailApprovedLeave($request))
                    return $this->successRes($store, msgUpdated());
                else
                    return $this->successRes($store, 'Data has been successfully updated, but failed to send email');
            }

            return $this->successRes($store, msgUpdated());
        }else{
            return $this->errorRes(msgNotUpdated());
        }
    }

    public function sendMailApprovedLeave($request)
    {
        $data = [];
        $ccList = $request->sdm_email;
        $ccList[] = $request->responder_email;

        $text = $request->status == 2 ? 'Canceled by SDM' : 'Approved';
        $data['subject'] = 'Your Work Permit Request Has Been '.$text;
        $data['email_to'] = $request->requester_email;
        $data['email_cc'] = $ccList;
        $data['email_recipient_name'] = $request->requester_name;
        $data['requester_name'] = $request->requester_name;
        $data['requester_id'] = $request->requester_id;
        $data['requester_initial'] = $request->requester_initial;
        $data['form_start_date'] = $request->start_date;
        $data['form_end_date'] = $request->end_date;
        $data['form_type'] = $request->atype_desc;
        $data['form_isfullday'] = $request->isfullday;
        $data['form_isfullday_desc'] = $request->isfullday == 1 ? 'Full Day' : 'Half Day';
        $data['form_event'] = $request->event ? $request->event == 'OUT' ? 'Clock Out' : 'Clock In' : '';
        $data['form_total_days'] = $request->total_days;
        $data['form_reason'] = $request->reason;
        $data['status_desc'] = $request->status == 2 ? 'canceled by SDM' : 'approved';
        $data['status'] = $request->status == 2 ? 'Canceled' : 'Approved';
        $data['responder_name'] = $request->responder_name;
        $data['responder_time'] = $request->status == 2 ? $request->canceled_time : ($request->approved_time ?: $request->approved_sdm_time);

        Mail::send('emails.permit-approved', ['data' => $data], function ($message) use ($data) {
            $message->subject($data['subject']);
            $message->to($data['email_to'], $data['email_recipient_name']);
            $message->cc($data['email_cc']);
        });

        if (!Mail::failures()) {
            return true;
        }

        return false;
    }

    public function sendMailApprovalLeave($request)
    {
        $data = [];
        $ccList = [];
        $to = '';
        $emailName = 'SDM';
        if($request->is_sdm_approver == 1){
            $to = $request->sdm_email;
            $ccList[] = $request->superior_email;
            $ccList[] = $request->requester_email;
        } else {
            $to = $request->superior_email;
            $emailName = $request->superior_name;
        }

        $data['subject'] = 'New Leave Request';
        $data['email_to'] = $to;
        $data['email_cc'] = $ccList;
        $data['email_recipient_name'] = $emailName;
        $data['requester_name'] = $request->requester_name;
        $data['requester_id'] = $request->requester_id;
        $data['requester_initial'] = $request->requester_initial;
        $data['form_end_date'] = $request->end_date;
        $data['form_start_date'] = $request->start_date;
        $data['form_type'] = $request->atype_desc;
        $data['form_isfullday'] = $request->isfullday;
        $data['form_isfullday_desc'] = $request->isfullday == 1 ? 'Full Day' : 'Half Day';
        $data['form_event'] = $request->event ? $request->event == 'OUT' ? 'Clock Out' : 'Clock In' : '';
        $data['form_total_days'] = $request->total_days;
        $data['form_reason'] = $request->note;
        $data['form_type_ketidakhadiran'] = $request->type_ketidakhadiran;
        $data['status_desc'] = $request->status == 2 ? 'canceled by SDM' : 'approved';
        $data['status'] = $request->status == 2 ? 'Canceled' : 'Approved';
        $data['responder_name'] = $request->responder_name;
        $data['is_sdm_approver'] = $request->is_sdm_approver;
        $data['responder_time'] = $request->status == 2 ? $request->canceled_time : $request->approved_time;
        $data['form_link'] = env('FE_URL','https://cbhrm-dev.prasetiyamulya.ac.id/').'/#/approval/work-permit/'.$request->token;

        Mail::send('emails.permit-approval', ['data' => $data], function ($message) use ($data) {
            $message->subject($data['subject']);
            $message->to($data['email_to'], $data['email_recipient_name']);
            $message->cc($data['email_cc']);
        });

        if (!Mail::failures()) {
            return true;
        }

        return false;
    }

    public function fetchMasterKetidakhadiran(Request $request)
    {
        $param = [];
//        $param['active'] = 1;
        if(isset($request->all()['subty']))
            $param['subty'] = $request->all()['subty'];
        if(isset($request->all()['type_ketidakhadiran']))
            $param['type_ketidakhadiran'] = $request->all()['type_ketidakhadiran'];

        $WorkPermitServices = $this->workPermitServices->fetchAllKetidakhadiran($param);
        foreach ($WorkPermitServices as $index => $key){
            $key->index = $index+1;
        }
        if ($WorkPermitServices)
            return $this->successRes($WorkPermitServices, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Master Kehadiran'), 404);

    }

    public function fetchQuota(Request $request)
    {
        $param = [];
        if(isset($request->all()['pernr']))
            $param['pa2006.pernr'] = $request->all()['pernr'];
        if(isset($request->all()['search']))
            $param['search'] = $request->all()['search'];
        if(isset($request->all()['period']))
            $param['period'] = $request->all()['period'];
        if(isset($request->all()['desta']))
            $param['pa2006.desta'] = $request->all()['desta'];
        if(isset($request->all()['deend']))
            $param['pa2006.deend'] = $request->all()['deend'];

        $array = [];
        $leaveServices = $this->workPermitServices->fetchLeaveQuota($param);
        foreach ($leaveServices as $index => $key){
            $key->index = $index+1;
        }
        if ($leaveServices)
            return $this->successRes($leaveServices, msgFetch(), 200);

        return $this->errorRes(msgNotFound('Leave Type'), 404);

    }

    public function batchPostData(Request $request,$id)
    {
        $files = $request->file('files');
        $arrayKetidakhadiran = [];
        $arrayRequest = [];
        $destination = 'upload';
        if($request->hasFile('files'))
        {
            foreach ($files as $file) {
                $rows = Excel::toCollection(new PermitImport, $file);

                $file->move($destination, $file->getClientOriginalName());

                $dateNow = date('Y-m-d H:i:s');
                for ($i = 0; $i < count($rows[0]); $i++) {
                    $startdate = Date::excelToDateTimeObject($rows[0][$i]['startdate'])->format('Y-m-d').' '.intval($rows[0][$i]['starttime']*24).':'.(intval($rows[0][$i]['starttime']*1440) - 60*intval($rows[0][$i]['starttime']*24)).':00';
                    $enddate = Date::excelToDateTimeObject($rows[0][$i]['enddate'])->format('Y-m-d').' '.intval($rows[0][$i]['endtime']*24).':'.(intval($rows[0][$i]['endtime']*1440) - 60*intval($rows[0][$i]['endtime']*24)).':00';
                    $datetime1 = new DateTime($enddate);
                    $datetime2 = new DateTime($startdate);
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');
                    $arrayKetidakhadiran[] = [
                        'beguz' => $startdate,
                        'enduz' => $enddate,
                        'pernr' => $rows[0][$i]['nik'],
                        'subty' => $rows[0][$i]['subty'],
                    ];
                    $arrayRequest[] = [
                        'start_date' => $startdate,
                        'end_date' => $enddate,
                        'pernr' => $rows[0][$i]['nik'],
                        'status' => 1,
                        'approved_sdm_time' => $dateNow,
                        'created_At' => $dateNow,
                        'updated_at' => $dateNow,
                        'approved_by_sdm_id' => $id,
                        'subty' => $rows[0][$i]['subty'],
                        'type_ketidakhadiran' => 5, // SDM batch
                        'duration' => $days == 0 ? 1 : $days, // SDM batch
                    ];
                }
            }
        }

        $store = $this->workPermitServices->batchPostData($arrayKetidakhadiran,$arrayRequest);

        if($store){
            return $this->successRes($store, msgStored());
        }else{
            return $this->errorRes(msgNotStored());
        }
    }
}
