<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    protected $table = 'permit_request';
    protected $fillable = ['id', 'requester_id', 'receiver_id', 'status', 'created_at', 'updated_at'];
}
