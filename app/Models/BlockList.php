<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockList extends Model
{
    protected $table = 'block_list';
    protected $fillable = ['id', 'user_id', 'block_user_id', 'created_at', 'updated_at'];
}
