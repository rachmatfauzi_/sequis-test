<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $table = 'friend';
    protected $fillable = ['id', 'user_id', 'friend_id', 'created_at', 'updated_at'];
}
