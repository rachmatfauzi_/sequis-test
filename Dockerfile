FROM php:7.4-apache

RUN apt-get update && \
    apt-get install -y vim libfreetype6-dev libzip-dev libjpeg62-turbo-dev libpng-dev libsodium-dev \
        libmcrypt-dev \
        libzip-dev \
        libonig-dev \
        libgettextpo-dev \
        libexif-dev \
        libbz2-dev \
        libicu-dev \
        libssl-dev \
        libxml2-dev && \
    docker-php-ext-install \
        gettext \
        exif \
        bz2 \
        calendar \
        mbstring \
        xml


RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) gd pdo_mysql zip sodium

RUN a2enmod rewrite

RUN echo "extension=pdo_mysql.so" > /usr/local/etc/php/conf.d/docker-php-ext-pdo_mysql.ini
RUN echo "extension=sodium.so" > /usr/local/etc/php/conf.d/docker-php-ext-sodium.ini
RUN echo "extension=zip.so" > /usr/local/etc/php/conf.d/docker-php-ext-zip.ini

WORKDIR /var/www/html

COPY . /var/www/html

COPY composer.lock composer.json /var/www/html/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && cd /var/www/html \
    && composer install

RUN cp /var/www/html/.env.example /var/www/html/.env
RUN php artisan key:generate
#RUN php artisan migrate --force --seed
RUN php artisan config:cache
RUN php artisan route:cache
RUN php artisan optimize:clear

RUN cd /var/www/html && chown -R www-data:www-data *

RUN sed -i 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf

RUN a2enmod rewrite
