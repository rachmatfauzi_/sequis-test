<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'full_name' => 'Andy Lau',
                'password' => 'a',
                'employee_id' => '123',
                'email' => 'andy@example.com'
            ],
            [
                'full_name' => 'John Doe',
                'password' => 'a',
                'employee_id' => '124',
                'email' => 'john@example.com'
            ],
            [
                'full_name' => 'Grace Full',
                'password' => 'a',
                'employee_id' => '125',
                'email' => 'grace@example.com'
            ],
            [
                'full_name' => 'Joe Taslim',
                'password' => 'a',
                'employee_id' => '126',
                'email' => 'joe@example.com'
            ],
        ]);
    }
}
