<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FriendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friend')->insert([
            [
                'user_id' => '3',
                'friend_user_id' => '4',
            ],
            [
                'user_id' => '2',
                'friend_user_id' => '4',
            ],
            [
                'user_id' => '3',
                'friend_user_id' => '1',
            ],
            [
                'user_id' => '2',
                'friend_user_id' => '1',
            ],
        ]);
    }
}
