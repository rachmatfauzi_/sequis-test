<?php

use Database\Seeders\UsersSeeder;
use Database\Seeders\FriendRequestSeeder;
use Database\Seeders\FriendSeeder;
use Database\Seeders\BlockListSeeder;
use Database\Seeders\StatusSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(FriendRequestSeeder::class);
        $this->call(FriendSeeder::class);
        $this->call(BlockListSeeder::class);
        $this->call(StatusSeeder::class);
    }
}
