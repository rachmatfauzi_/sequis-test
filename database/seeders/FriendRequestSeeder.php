<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FriendRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friend_request')->insert([
            [
                'requester_id' => '3',
                'receiver_id' => '4',
                'status' => '1',
            ],
            [
                'requester_id' => '3',
                'receiver_id' => '4',
                'status' => '2',
            ],
            [
                'requester_id' => '3',
                'receiver_id' => '1',
                'status' => '3',
            ],
        ]);
    }
}
