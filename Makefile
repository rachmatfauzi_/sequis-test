setup:
	@make build
	@make up
build:
	docker-compose build --no-cache --force-rm
stop:
	docker-compose stop
up:
	docker-compose up -d
data:
	docker exec api bash -c "php /var/www/html/artisan migrate --force"
	docker exec api bash -c "php /var/www/html/artisan db:seed"
